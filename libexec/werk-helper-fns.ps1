function Iex-Native {
    # execute native commands using Iex without errors

    [CmdletBinding()]
    Param(
        [Parameter(
             Mandatory = $True,
             ValueFromPipeline = $true)]
        [String] $Cmd)

    $Cmd | iex 2>&1 | %{ "$_" }
}

# import functions to programmatically generate icons
. "$PSScriptRoot\make-icon.ps1"

function Export-RegistryValues ([string]$Path, [string[]]$RegistryValues, [string]$ExportPath) {

    If (-not ($ExportPath | Test-Path)) {
        Create-DirectoryIfNotExist $ExportPath
    }
    Push-Location $ExportPath

    $copyPath = $Path + 'Export'
    $regKey = (Split-Path -Leaf $Path)
    $regExePath = $Path.Replace(':', '') # reg.exe doesn't use the ':' notation in path, but PowerShell's *-ItemProperty does
    $regExeCopyPath = $regExePath + 'Export'
    $regExportKey = (Split-Path -Leaf $copyPath)
    #$regExportFilename = $regExportKey + '.reg'
    $regExportFilename = $regExeCopyPath.Replace('\','-') + '.reg' # quick and dirty hack to get around not excluding child registry entries (will mean lots more, unnecessary reg files)
    $valsToRemove = Get-ItemProperty $Path | Select * -Exclude $RegistryValues | Select * -Exclude PS* # find the inverse of the 'RegistryValues' list
    $valsToRemove = $valsToRemove.PSObject.Properties.Name

    try {
        # copy the existing environment key
        reg copy $regExePath $regExeCopyPath /s /f
  
        # filter out the values we dont need
        Remove-ItemProperty -Path $copyPath -Name $valsToRemove
        reg export $regExeCopyPath $regExportFilename /y
        (Get-Content $regExportFilename) -replace $regExportKey, $regKey | Set-Content $regExportFilename

  
    }
    finally {
        # make sure we get rid of the copied key if anything goes wrong
        reg delete $regExeCopyPath /f 
    }

    Pop-Location
  
}

# INVESTIGATE INCORPORATING INTO EXPORT-REGISTRYVALUES FUNCTION - for removing a path of it is a child of a parent path
# open question - how to determine the parent paths to check?
# from: https://www.reddit.com/r/PowerShell/comments/333lxv/how_to_check_if_a_path_is_a_child_of_another/

# $shares = $shares | Sort-Object Path

# forEach ($share in $shares) {
#     if ($share.Path -eq "") {
#         continue
#     }

#     #check if path is already present
#     if($sharesCulled.Path -Contains $share.Path) {
#         Write-Host $share.Name "on" $server " is a duplicate of another share."
#     }
#     else {
#         $parent = $share.Path
#         while ($true){
#             $parent = $parent | Split-Path -parent
#             if ($parent -eq "") {
#                 $sharesCulled += $share
#                 break
#             }
#             if ($sharesCulled.Path -Contains $parent){
#                 Write-Host $share.Name "on" $server " is a nested share"
#                 break
#             }
#         }
#     }
# }


# OTHER RANDOM CAN PROBABLY DELETE

# get parent of registry key
# Get-ItemProperty ((Get-ItemProperty $entry).PSParentPath)

function Create-DirectoryIfNotExist {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $True)]
        [String] $DirectoryToCreate)

    if (-not (Test-Path -LiteralPath $DirectoryToCreate)) {
      
        try {
            New-Item -Path $DirectoryToCreate -ItemType Directory -ErrorAction Stop | Out-Null #-Force
        }
        catch {
            Write-Error -Message "Unable to create directory '$DirectoryToCreate'. Error: $_" -ErrorAction Stop
        }
        "Successfully created directory '$DirectoryToCreate'."

    }
    else {
        "Directory '$DirectoryToCreate' exists."
    }
}

function Write-Step {
    [CmdletBinding(DefaultParameterSetName = 'Message')]
    param(

        # $Object (aka Message, or the string) is mandatory regardless of parameter set
        [Parameter(ParameterSetName = 'Message', Mandatory = $true)]
        [Parameter(ParameterSetName = 'SubStep', Mandatory = $true)]
        [Parameter(ParameterSetName = 'AsError', Mandatory = $true)]
        [Parameter(ParameterSetName = 'AsSuccess', Mandatory = $true)]
        [Parameter(ParameterSetName = 'AsTimer', Mandatory = $true)]
        [Parameter(Position = 0)]
        [Object]$Object,

        # parameter to indicate a 'sub-step' should be available to all but mandatory to none (except sub-step)
        [Parameter(ParameterSetName = 'SubStep', Mandatory = $true)]
        [Parameter(ParameterSetName = 'Message', Mandatory = $false)]
        [Parameter(ParameterSetName = 'AsError', Mandatory = $false)]
        [Parameter(ParameterSetName = 'AsSuccess', Mandatory = $false)]
        [Parameter(ParameterSetName = 'AsTimer', Mandatory = $false)]
        [switch]$Sub,

        # error has pre-set colours, no other options
        [Parameter(ParameterSetName = 'AsError', Mandatory = $true)]
        [switch]$AsError,

        # success has pre-set colours, no other options
        [Parameter(ParameterSetName = 'AsSuccess', Mandatory = $true)]
        [switch]$AsSuccess,

        # timer has pre-set colours, no other options
        [Parameter(ParameterSetName = 'AsTimer', Mandatory = $true)]
        [switch]$AsTimer,

        # only allow changing fg for default 'Message' parameter or substep
        [Parameter(ParameterSetName = 'Message', Mandatory = $false)]
        [Parameter(ParameterSetName = 'SubStep', Mandatory = $false)]
        [ConsoleColor]$ForegroundColor = 'Black',

        # and bg only on messages (as substeps dont have these)
        [Parameter(ParameterSetName = 'Message', Mandatory = $false)]
        [ConsoleColor]$BackgroundColor = 'Yellow'
    )

    $opts = @{'ForegroundColor' = ''; 'BackgroundColor' = '' }
    $paramSet = $PSCmdlet.ParameterSetName
    $Object = "`n  {0}  `n >>> `n" -f $Object
  
    if ($AsError) {
        # pre-define colours for error
        if ($sub) { $ForegroundColor = 'Red' }
        else { $ForegroundColor = 'White'; $BackgroundColor = 'Red' }
    }

    if ($AsSuccess) {
        # pre-define colours for success
        if ($sub) { $ForegroundColor = 'Green' }
        else { $ForegroundColor = 'White'; $BackgroundColor = 'Green' }
    }

    if ($AsTimer) {
        # pre-define colours for success
        if ($sub) { $ForegroundColor = 'Gray' }
        else { $ForegroundColor = 'Black'; $BackgroundColor = 'Gray' }
    }

    if ($sub) {
        $opts.Remove('BackgroundColor') # remove background color for substeps
        if ($foregroundColor) { $opts['ForegroundColor'] = $ForegroundColor }
        else { $opts['ForegroundColor'] = 'Yellow' } # set foreground to white if no colour specified (for visibility on default powershell blue)
        # default terminal colour (found it a little easy to miss): (get-host).ui.rawui.ForegroundColor
    }
  
    else { 
        $opts['ForegroundColor'] = $ForegroundColor
        $opts['BackgroundColor'] = $BackgroundColor
    }

    Write-Host -Object $Object @opts

}


## Adapted from: https://github.com/chocolatey/chocolatey ('Set-EnvironmentVariables.ps1')

function Get-UserEnvVar ([string]$EnvName, [switch]$ExpandVars) {
    # gets the environment variable directly from the registry, with option to keep %vars% un-expanded

    # use win32 API to access the registry key
    $regHive = [Microsoft.Win32.Registry]::CurrentUser
    $envRegKey = $regHive.OpenSubKey("Environment", [Microsoft.Win32.RegistryKeyPermissionCheck]::ReadWriteSubTree)

    # define option to not expand paths
    $pathExpansion = if (-not $ExpandVars) { [Microsoft.Win32.RegistryValueOptions]::DoNotExpandEnvironmentNames } else { [Microsoft.Win32.RegistryValueOptions]::None }

    # get the value with or without path expansion and return
    return $envRegKey.GetValue($EnvName, $null, $pathExpansion)

}

function Refresh-EnvironmentVariables {
    try {
        # make everything refresh
        # because sometimes explorer.exe just doesn't get the message that things were updated.
        if (-not ("win32.nativemethods" -as [type])) {
            # import sendmessagetimeout from win32
            add-type -Namespace Win32 -Name NativeMethods -MemberDefinition @"
[DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
public static extern IntPtr SendMessageTimeout(
    IntPtr hWnd, uint Msg, UIntPtr wParam, string lParam,
    uint fuFlags, uint uTimeout, out UIntPtr lpdwResult);
"@
        }

        $HWND_BROADCAST = [intptr]0xffff;
        $WM_SETTINGCHANGE = 0x1a;
        $result = [uintptr]::zero

        # notify all windows of environment block change
        [win32.nativemethods]::SendMessageTimeout($HWND_BROADCAST, $WM_SETTINGCHANGE, [uintptr]::Zero, "Environment", 2, 5000, [ref]$result) | Out-Null

        # Set a user environment variable making the system refresh
        $setx = "$($env:SystemRoot)\System32\setx.exe"
        & "$setx" propositumLastUpdateEnvs `"$((Get-Date).ToFileTime())`" | Out-Null
    }
    catch {
        Write-Warning "Failure attempting to let Explorer know about updated environment settings.`n  $($_.Exception.Message)"
    }
}

# From: https://serverfault.com/questions/95431/in-a-powershell-script-how-can-i-check-if-im-running-with-administrator-privilfunction Test-Administrator  
function Test-Administrator {  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
}

# from: https://rosettacode.org/wiki/Convert_seconds_to_compound_duration#PowerShell
function Get-Time
{
  <#
    .SYNOPSIS
        Gets a time string in the form: # wk, # d, # hr, # min, # sec
    .DESCRIPTION
        Gets a time string in the form: # wk, # d, # hr, # min, # sec
        (Values of 0 are not displayed in the string.)
 
        Days, Hours, Minutes or Seconds in any combination may be used
        as well as Start and End dates.
 
        When used with the -AsObject switch an object containing properties
        similar to a System.TimeSpan object is returned.
    .INPUTS
        DateTime or Int32
    .OUTPUTS
        String or PSCustomObject
    .EXAMPLE
        Get-Time -Seconds 7259
    .EXAMPLE
        Get-Time -Days 31 -Hours 4 -Minutes 8 -Seconds 16
    .EXAMPLE
        Get-Time -Days 31 -Hours 4 -Minutes 8 -Seconds 16 -AsObject
    .EXAMPLE
        Get-Time -Start 3/10/2016 -End 1/20/2017
    .EXAMPLE
        Get-Time -Start (Get-Date) -End (Get-Date).AddSeconds(6000000)
  #>
    [CmdletBinding(DefaultParameterSetName='Date')]
    Param
    (
        # Start date
        [Parameter(Mandatory=$false, ParameterSetName='Date',
                   Position=0)]
        [datetime]
        $Start = (Get-Date),
 
        # End date
        [Parameter(Mandatory=$false, ParameterSetName='Date',
                   Position=1)]
        [datetime]
        $End = (Get-Date),
 
        # Days in the time span
        [Parameter(Mandatory=$false, ParameterSetName='Time')]
        [int]
        $Days = 0,
 
        # Hours in the time span
        [Parameter(Mandatory=$false, ParameterSetName='Time')]
        [int]
        $Hours = 0,
 
        # Minutes in the time span
        [Parameter(Mandatory=$false, ParameterSetName='Time')]
        [int]
        $Minutes = 0,
 
        # Seconds in the time span
        [Parameter(Mandatory=$false, ParameterSetName='Time')]
        [int]
        $Seconds = 0,
 
        [switch]
        $AsObject
    )
 
    Begin
    {
        [PSCustomObject]$timeObject = "PSCustomObject" |
            Select-Object -Property Weeks,RemainingDays,
                                    Days,Hours,Minutes,Seconds,Milliseconds,Ticks,
                                    TotalDays,TotalHours,TotalMinutes,TotalSeconds,TotalMilliseconds
        [int]$remainingDays  = 0
        [int]$weeks          = 0
 
        [string[]]$timeString = @()
    }
    Process
    {
        switch ($PSCmdlet.ParameterSetName)
        {
            'Date' { $timeSpan = New-TimeSpan -Start $Start -End $End }
            'Time' { $timeSpan = New-TimeSpan -Days $Days -Hours $Hours -Minutes $Minutes -Seconds $Seconds }
        }
 
        $weeks = [System.Math]::DivRem($timeSpan.Days, 7, [ref]$remainingDays)        
 
        $timeObject.Weeks             = $weeks
        $timeObject.RemainingDays     = $remainingDays
        $timeObject.Days              = $timeSpan.Days
        $timeObject.Hours             = $timeSpan.Hours
        $timeObject.Minutes           = $timeSpan.Minutes
        $timeObject.Seconds           = $timeSpan.Seconds
        $timeObject.Milliseconds      = $timeSpan.Milliseconds
        $timeObject.Ticks             = $timeSpan.Ticks
        $timeObject.TotalDays         = $timeSpan.TotalDays
        $timeObject.TotalHours        = $timeSpan.TotalHours
        $timeObject.TotalMinutes      = $timeSpan.TotalMinutes
        $timeObject.TotalSeconds      = $timeSpan.TotalSeconds
        $timeObject.TotalMilliseconds = $timeSpan.TotalMilliseconds
    }
    End
    {
        if ($AsObject) { return $timeObject }
 
        if ($timeObject.Weeks)         { $timeString += "$($timeObject.Weeks) wk"        }
        if ($timeObject.RemainingDays) { $timeString += "$($timeObject.RemainingDays) d" }
        if ($timeObject.Hours)         { $timeString += "$($timeObject.Hours) hr"        }
        if ($timeObject.Minutes)       { $timeString += "$($timeObject.Minutes) min"     }
        if ($timeObject.Seconds)       { $timeString += "$($timeObject.Seconds) sec"     }
 
        return ($timeString -join ", ")
    }
}

function Write-TimeElapsed ($StpWch) {
    $timeElapsed = Get-Time -Seconds $StpWch.Elapsed.TotalSeconds
    If (!$timeElapsed) {$timeElapsed = '< 1 sec'}
    $msg = ' >>>>> Time Taken: {0} ' -f $timeElapsed
    Write-Host -BackgroundColor 'Gray' -ForegroundColor 'Black' $msg
}
