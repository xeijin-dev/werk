# derived from: @jfarre20 - https://www.reddit.com/r/PowerShell/comments/3wtjr7/question_powershell_script_to_create_profile/
 function Make-Icon ([string]$Category, [string]$Name, [int]$Size=256, [string]$Type, [switch]$Base64, [array]$ForceColour) {
    # programmatically generate an icon from a string
    # Category is an optional value (e.g. application name) that ensures a consistent colour is generated

    # add the required windows assembly
    Add-Type -AssemblyName System.Drawing
    $TextInfo = (Get-Culture).TextInfo
    $Name = $TextInfo.ToLower($Name)

    $Name = $Name.Split([IO.Path]::GetInvalidFileNameChars()) -join '-' # remove any illegal chars
    $NameParts = $Name.Split(' ').Split('-').Split('_') # split into array

    $IconNames = $NameParts | %{ Get-IconStr $_ } # return the 'short' name of each string in the array

    # 30 colour blind friendly, pastel colours generated from: http://medialab.github.io/iwanthue/
    #$PastelsRgbArr = @(138,179,234),@(143,185,176),@(146,187,243),@(161,240,241),@(184,177,243),@(187,188,140),@(226,201,240),@(168,186,214),@(131,212,245),@(149,196,154),@(123,192,243),@(229,230,180),@(94,196,243),@(188,230,171),@(166,183,243),@(80,210,221),@(168,174,220),@(158,187,228),@(132,200,252),@(223,186,149),@(167,210,201),@(224,178,223),@(241,176,179),@(154,196,252),@(189,239,195),@(133,226,208),@(150,190,214),@(193,198,246),@(122,180,249),@(193,211,239)
    #$RandPastel = ,$PastelsRgbArr | Get-Random

    # assign a colour unless the user specifies
    If ($ForceColour -is 'Array') {
        $StrAsColour = $ForceColour
    }
    Else {
        $StrAsColour = Make-ColourFromStr $Category # generates a colour unique to the string
    }

    $Colour = [System.Drawing.Color]::FromArgb($StrAsColour[0], $StrAsColour[1], $StrAsColour[2])
    
    
    $filename =  (Get-Location).Path + "\" + ($NameParts -join '-')
    $fontSize = $Size * 0.25
    $borderWidth = $Size * 0.06
    $mainBgSize = $Size - $borderWidth
    $txtOffset = $borderWidth * 1.1

    # create bitmap & font objects
    $bmp = new-object System.Drawing.Bitmap $Size,$Size
    $font = new-object System.Drawing.Font ("Arial",$fontSize,[System.Drawing.FontStyle]::Bold)

    # initialise brush objects
    $pen = new-object Drawing.Pen Black
    $pen.width = $Size * 0.03
    $brushBrd = [System.Drawing.Brushes]::Black
    $brushBg = new-object System.Drawing.SolidBrush($Colour)
    $brushFg = [System.Drawing.Brushes]::White

    # initialise a drawing surface
    $drawingSurface = [System.Drawing.Graphics]::FromImage($bmp)

    # set options for better quality output
    $drawingSurface.SmoothingMode = "AntiAlias"
    $drawingSurface.PixelOffsetMode = "HighQuality"
    $drawingSurface.InterpolationMode = "HighQualityBicubic"

    # paint a background (which will eventually be our 2px border)
    $drawingSurface.FillRectangle($brushBrd,0,0,$bmp.Width,$bmp.Height)

    # paint a slightly narrower background (which will be our main bg colour)
    $drawingSurface.FillRectangle($brushBg,($borderWidth / 2),($borderWidth / 2),$mainBgSize,$mainBgSize)
    
    # add the line for decoration
    $drawingSurface.DrawLine($pen, ($Size * 0.3), ($Size * 0.82), $Size, ($Size * 0.82))

    # determine how many lines of text to add
    If ($IconNames -IsNot "array") {
        $firstLine = $IconNames
        $secondLine = $null
    }
    ElseIf ($Size -gt 16)
    {
        $firstLine = $IconNames[0]
        $secondLine = $IconNames[$IconNames.GetUpperBound(0)]
    }

    # add the text
    If ($Size -gt 16) { 
        $drawingSurface.DrawString($firstLine,$font,$brushFg,$txtOffset,$txtOffset) # align top row slightly to the left
        If ($secondLine) {
            $drawingSurface.DrawString($secondLine,$font,$brushFg,$txtOffset,(0.9*($fontSize*1.7)))
        }
    }
    Else { # special handling for 16x16 icon given resolution impairs readability
        If (-not $firstLine) {
            $firstLine = $IconNames[0][0] + $IconNames[1][0]
        }
        $font.Dispose()
        $font = new-object System.Drawing.Font ("Arial",6,[System.Drawing.FontStyle]::Bold)
        $drawingSurface.DrawString($firstLine,$font,$brushFg,1,1)
    }

    # save into file
    If ($Type -eq "png") {
        $filename = $filename + ".png"
        $bmp.Save($filename)
    }
    Else { # conver to ico
        # clean-up / generate file
        $filename = $filename + ".ico"
        $icon = [System.Drawing.Icon]::FromHandle($bmp.GetHicon())
        $file = [IO.File]::Create("$filename")
        $icon.Save($file)
        $icon.Dispose()
        $file.Close()
    }

    If ($base64) {
        $result = @()
        $result += $filename
        $result += [Convert]::ToBase64String([IO.File]::ReadAllBytes($FileName))
    }
    Else {$result = $filename}

    $drawingSurface.Dispose()
    $bmp.Dispose()
    $font.Dispose()

    # dbg
    #Write-Host "Icon created: '$filename'"
    return $result
    # result[1] = filename
    # result[2] = base64
    #Invoke-Expression "$filename"
    #explorer "/SELECT,$filename"
}

function Make-ColourFromStr ([string]$str) {

    $strHash = Get-FNVHash $str # random, purely numeric seed unique to the provided string

    $num = $strHash
    $mixer = 50 # value used to 'lighten' a given colour so we get a pastel

    # randomise the RGB values with a cap to 128, then add mixer to get pastel colour
    $r = (Get-Random -Minimum 0 -Maximum 128 -SetSeed ($num -shr 16)) + $mixer
    $g = (Get-Random -Minimum 0 -Maximum 128 -SetSeed ($num -shr 7 -band 255)) + $mixer
    $b = (Get-Random -Minimum 0 -Maximum 128 -SetSeed ($num -band 255)) + $mixer

    return @($r, $g, $b)

}

function Get-IconStr ([string]$str) { # returns 3 letters of provided string, stripping out vowels (unless word is only 3 letters)
# derived from: @bis - https://www.reddit.com/r/PowerShell/comments/9olsg8/selecting_first_letter_of_a_string_then_the_first/e7vj03u
    $res = ""
    $TextInfo = (Get-Culture).TextInfo
    $consonants = 'bcdfghjklmnpqrstvwxz'

    If ($str.length -le 3) {$res = $str} # dont do extraction if 3 letters or less

    Else {
        $res = $str -replace "(?<!^)[^$consonants]*" -replace '(.)(?=.*\1)' -replace '(?<=^...).*'
        # first replace: remove everything except 1st char & consonants
        # second replace: only keep the last instance of duplicated characters (e.g. aa and aaa both return 'a')
        
        If ($res.Length -lt 2) {$res = $str} # for very short strings dont bother stripping out vowels, use as-is
    }

    Return $TextInfo.ToTitleCase($res)
}

function Get-FNVHash { # provides a unique, numeric hash, given a string
    # source: https://stackoverflow.com/questions/45881037/powershell-convert-unique-string-to-unique-int

    param(
        [string]$InputStr
    )

    # Initial prime and offset chosen for 32-bit output
    # See https://en.wikipedia.org/wiki/Fowler–Noll–Vo_hash_function
    [uint32]$FNVPrime = 16777619
    [uint32]$offset = 2166136261

    # Convert string to byte array, may want to change based on input collation
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($InputStr)

    # Copy offset as initial hash value
    [uint32]$hash = $offset

    foreach($octet in $bytes)
    {
        # Apply XOR, multiply by prime and mod with max output size
        $hash = $hash -bxor $octet
        $hash = $hash * $FNVPrime % [System.Math]::Pow(2,32)
    }
    return $hash
}



