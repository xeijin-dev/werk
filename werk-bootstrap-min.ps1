# bare minimum bootstrap of emacs + doom-emacs + doom-config
New-Item -Path "C:\" -Name "tst" -ItemType "directory"
iex 'subst P: C:\tst'
$env:SCOOP = "P:/"
iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
iex 'scoop bucket add extras'
iex 'scoop bucket add propositum https://gitlab.com/xeijin-dev/propositum-bucket.git/'
iex 'scoop update'
iex 'scoop install emacs-p'
iex 'scoop install fd'
Set-Alias -Name find -Value fd.exe # prevent straight.el complaining about failing to run 'find'
Push-Location 'P:\'
iex 'git clone --verbose --progress https://gitlab.com/xeijin-dev/doom-config.git .doom.d' 2>&1 | %{ "$_" }
iex 'git clone --verbose --progress https://github.com/hlissner/doom-emacs .emacs.d' 2>&1 | %{ "$_" }
$env:HOME = "P:/"
$env:EMACSDIR = "P:/.emacs.d/"
$env:DOOMDIR = "P:/.doom.d/"
Push-Location "P:\.emacs.d\bin"
.\doom.cmd -y refresh 2>&1 | %{ "$_" }

$artifactName = 'werk-MINIMAL' + '-' + (Get-Date -format 'yyyy-MMM-dd') + '-' + -join ($env:APPVEYOR_REPO_COMMIT.ToCharArray() | Select-Object -First 8) # give it a unique name with the date + first 5 letters of commit

Write-Host "Compressing files into release artifact..."
Set-Location "P:/" # cd to root, as 7z -v switch does not support specifying end file and directory

Write-Host "Creating TAR archive..."
"7z a -bsp1 -ttar -snl {0}.tar ./" -f $artifactName | iex 2>&1 | %{ "$_" }  # Create tar archive to preserve symlinks

Write-Host "Compressing TAR into 7z archive..."
"7z a -bsp1 -t7z {0}.tar.7z {0}.tar -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on -v1500m" -f $artifactName | iex 2>&1 | %{ "$_" } # Compress tar into 7z archive

$blockRdp = $true; iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
#emacs --batch -l $env:EMACSDIR/init.el -l emacsql-sqlite --eval --% "(progn (add-to-list 'exec-path \"C:/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0\") (setenv \"PATH\" (mapconcat #'identity exec-path path-separator)) (emacsql-sqlite-ensure-binary))"
#$blockRdp = $true; iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
