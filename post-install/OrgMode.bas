Attribute VB_Name = "OrgMode"
' WinApi function that maps a UTF-16 (wide character) string to a new character string - fixed for 64-bit applications
' Note VBA complains unless this bit of code is at the top of the module?
Private Declare PtrSafe Function WideCharToMultiByte Lib "kernel32" ( _
    ByVal CodePage As LongPtr, _
    ByVal dwFlags As LongPtr, _
    ByVal lpWideCharStr As LongPtr, _
    ByVal cchWideChar As LongPtr, _
    ByVal lpMultiByteStr As LongPtr, _
    ByVal cbMultiByte As LongPtr, _
    ByVal lpDefaultChar As LongPtr, _
    ByVal lpUsedDefaultChar As LongPtr) As LongPtr

' CodePage constant for UTF-8
Private Const CP_UTF8 = 65001

' Late binding MS Forms 2.0 library - REQUIRED for clipboard functionality
' https://bytecomb.com/copy-and-paste-in-vba/
Const DATAOBJECT_BINDING As String = "new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}"

' Function & sub-routine to workaround character limits of VBA Shell command
Option Explicit
Private Declare PtrSafe Function ShellExecute _
                            Lib "shell32.dll" _
                            Alias "ShellExecuteA" ( _
                            ByVal hwnd As Long, _
                            ByVal lpOperation As String, _
                            ByVal lpFile As String, _
                            ByVal lpParameters As String, _
                            ByVal lpDirectory As String, _
                            ByVal nShowCmd As Long) _
                            As Long

Private Sub LaunchUrl(strUrl As String)
On Error GoTo wellsrLaunchError
    Dim r As Long
    r = ShellExecute(0, "open", strUrl, 0, 0, 1)
    If r = 5 Then 'if access denied, try this alternative
            r = ShellExecute(0, "open", "rundll32.exe", "url.dll,FileProtocolHandler " & strUrl, 0, 1)
    End If
    Exit Sub
wellsrLaunchError:
MsgBox "Error encountered while trying to launch URL." & vbNewLine & vbNewLine & "Error: " & Err.Number & ", " & Err.Description, vbCritical, "Error Encountered"
End Sub

' Better URL encode functions - entirely taken from http://stackoverflow.com/a/3812363/432152
Public Function URLEncode( _
   StringVal As String, _
   Optional SpaceAsPlus As Boolean = False, _
   Optional UTF8Encode As Boolean = True _
) As String

    Dim StringValCopy As String: StringValCopy = IIf(UTF8Encode, UTF16To8(StringVal), StringVal)
    Dim StringLen As Long: StringLen = Len(StringValCopy)
    
    If StringLen > 0 Then
        ReDim result(StringLen) As String
        Dim i As Long, CharCode As Integer
        Dim Char As String, Space As String
    
      If SpaceAsPlus Then Space = "+" Else Space = "%20"
    
      For i = 1 To StringLen
        Char = Mid$(StringValCopy, i, 1)
        CharCode = Asc(Char)
        Select Case CharCode
          Case 97 To 122, 65 To 90, 48 To 57, 45, 46, 95, 126
            result(i) = Char
          Case 32
            result(i) = Space
          Case 0 To 15
            result(i) = "%0" & Hex(CharCode)
          Case Else
            result(i) = "%" & Hex(CharCode)
        End Select
      Next i
      URLEncode = Join(result, "")
    
    End If
End Function


Public Function UTF16To8(ByVal UTF16 As String) As String
    Dim sBuffer As String
    Dim lLength As Variant
    If UTF16 <> "" Then
        lLength = WideCharToMultiByte(CP_UTF8, 0, StrPtr(UTF16), -1, 0, 0, 0, 0)
        sBuffer = Space$(lLength)
        lLength = WideCharToMultiByte(CP_UTF8, 0, StrPtr(UTF16), -1, StrPtr(sBuffer), Len(sBuffer), 0, 0)
        sBuffer = StrConv(sBuffer, vbUnicode)
        UTF16To8 = Left$(sBuffer, lLength - 1)
    Else
        UTF16To8 = ""
    End If
End Function

Public Function OPSanitise(str As String, Optional ByVal excludeSquareBrackets As Boolean)
' sanitises an org-protocol url by replacing equals signs (=) and ampersands (&)
    str = Replace(str, "=", ">>")
    str = Replace(str, "&", "+")

    ' option to remove square brackets for org-links where this messes up display/editing in org-mode
    If not excludeSquareBrackets Then
        str = Replace(str, "[", "{")
        str = Replace(str, "]", "}")
    End If

    OPSanitise = str
    
End Function

Sub OrgCaptureFromItem()
' Creates a new capture from the selected Outlook item, for meetings it will grab the attendees alongside start/end dates
' Note that capture for appointments/meetings is dependent upon a custom org-protocol handler, for accompanying elisp code see: https://gitlab.com/xeijin-dev/propositum
' Much code swiped from /u/rnadler: https://www.reddit.com/r/emacs/comments/915e5n/creating_outlook_appointments_from_org_mode/e2y6m1p/

    Dim orgProtocolCommand As String
    Dim objItem As Variant
    Dim winScript As Object ' to access the registry
    Dim outlookVer As String
    Dim ErrMsg As String
    Dim AllowCommasState As String
    Static AllowCommasWarned As Integer ' So we can persist the value for a given session
                     
                  
        ' Ensure we have a selected or open Outlook item
        If Not Application.ActiveExplorer Is Nothing Then
            If Application.ActiveExplorer.Caption = "Outlook Today - Outlook" Then ' Handle Outlook today which falsely masquerades as an ActiveExplorer view...
                    MsgBox ("This function is not supported from the Outlook Today view." & vbCrLf & vbCrLf & "Please open a folder and select an item.")
                    Exit Sub
            ElseIf Application.ActiveExplorer.Selection.Count > 1 Then
                MsgBox ("Please select one item at a time.")
                Exit Sub
            ElseIf Application.ActiveExplorer.Selection.Count <= 0 Then
                MsgBox ("Please select an item.")
            Exit Sub
            Else: ' Proceed if single item is selected
                Set objItem = Application.ActiveExplorer.Selection.Item(1)
            End If
        
        ElseIf Not Application.ActiveInspector Is Nothing Then ' Add support for open Outlook items (aka inspector view)
            Set objItem = Application.ActiveInspector.CurrentItem
        Else:
            MsgBox ("A suitable Outlook item could not be found. Try re-starting Outlook to see if the error persists.")
            Exit Sub
        End If
        
        ' Check registry to confirm if 'allow commas to separate recipients' is on (comma causes display names to separate, making the attendees list wrong) & prompt/warn user if necessary
        Set winScript = CreateObject("WScript.Shell")
        outlookVer = Left(Application.Version, 4)  ' get the current outlook version for registry operations
        On Error Resume Next ' If the key doesn't exist in the registry, we still have an issue so keep going
        AllowCommasState = winScript.RegRead("HKEY_CURRENT_USER\Software\Microsoft\Office\" & outlookVer & "\Outlook\Preferences\AllowCommasInRecip")
        
        If AllowCommasState <> 0 And AllowCommasWarned = 0 Then
            ErrMsg = MsgBox("The Outlook setting:" & vbCrLf & vbCrLf & "'Commas can be used to separate multiple message recipients'" & vbCrLf & vbCrLf & "is currently turned on. This can prevent org-outlook-plus from retrieving meeting attendees correctly." & vbCrLf & vbCrLf & "You can disable from within Outlook by navigating to:" & vbCrLf & vbCrLf & "'File > Options > Mail > Send messages'" & vbCrLf & vbCrLf & "Alternatively, the script can try and set this for you - go ahead and attempt this now?", vbYesNo & vbExclamation, "Warning")
                Select Case ErrMsg
                    Case Is = vbYes ' If user selects 'Yes' attempt to (over)write the registry value that controls the preference with '0' (disable)
                      Call winScript.RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Office\" & outlookVer & "\Outlook\Preferences\AllowCommasInRecip", 0, "REG_DWORD")
                    Case Is = vbNo
                      AllowCommasWarned = 1 ' Don't warn user again until next startup
                    Case Else
                      AllowCommasWarned = 1
                End Select
        End If
    
        ' Handle the different Outlook item types
        Select Case objItem.Class
        
                Case olMail
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=MAIL" & "&subject=" & OPSanitise(objItem.Subject) & "&sender=" & objItem.SenderName
                    
                Case olAppointment
                    If objItem.MeetingStatus <> olNonMeeting Then ' meetings use a customer org-protocol handler ('meeting')
                        orgProtocolCommand = "org-protocol:/outlook?template=zm&olid=outlook:" & objItem.EntryID & "&type=MEETING" & "&start=" & Format(objItem.Start, "yyyy-MM-dd hh:mm") & "&end=" & Format(objItem.End, "yyyy-MM-dd hh:mm") & "&subject=" & OPSanitise(objItem.Subject) & "&sender=" & OPSanitise(objItem.Organizer, True) & "&attendees=" & OPSanitise(objItem.RequiredAttendees, True) & "; " & OPSanitise(objItem.OptionalAttendees, True)
                    ElseIf objItem.Class = olAppointment And objItem.MeetingStatus = olNonMeeting Then ' appointments also use the custom org-protocol handler, but without sender/attendee fields
                        orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=APPT" & "&start=" & Format(objItem.Start, "yyyy-MM-dd hh:mm") & "&end=" & Format(objItem.End, "yyyy-MM-dd hh:mm") & "&subject=" & OPSanitise(objItem.Subject)
                    End If
                    
                Case olTask
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=TASK" & "&subject=" & OPSanitise(objItem.Subject) & "&sender=" & OPSanitise(objItem.Owner, True)
                    
                Case olContact
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=CONTACT" & "&subject=" & OPSanitise(objItem.Subject) & "&other=" & OPSanitise(objItem.FullName, True)
                    
                Case olJournal
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=JOURNAL" & "&subject=" & OPSanitise(objItem.Subject) & "&other=" & OPSanitise(objItem.Type, True)
                    
                Case olNote
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=NOTE" & "&subject=" & OPSanitise(objItem.Subject)
                    
                Case Else
                    orgProtocolCommand = "org-protocol:/outlook?template=zo&olid=outlook:" & objItem.EntryID & "&type=ITEM" & "&subject=" & OPSanitise(objItem.Subject) & "&other=" & OPSanitise(objItem.MessageClass, True)
            
        End Select
        
        
        ' Debugging
        'MsgBox (orgProtocolCommand)
        '' Clipboard stuff
        'Dim doClipboard As Object
        'Set doClipboard = CreateObject(DATAOBJECT_BINDING)
        'doClipboard.SetText orgProtocolCommand
        'doClipboard.PutInClipboard
        ' Old command, problems with character limits
        'Shell "C:\WINDOWS\explorer.exe """ & orgProtocolCommand
        
        ' Custom LaunchUrl function uses ShellExecute to get around character limits
        LaunchUrl (orgProtocolCommand)
        
         Set objItem = Nothing
         Set winScript = Nothing

End Sub

Sub CopyItemAsOrgLink()
'Adds a link to the currently selected item in org-mode link format
    Dim objItem As Object
    'was earlier Outlook.MailItem
    Dim doClipboard As Object
      
    'One and ONLY one message muse be selected
    If Application.ActiveExplorer.Selection.Count <> 1 Then
        MsgBox ("Select one and ONLY one message.")
        Exit Sub
    End If
   
    Set objItem = Application.ActiveExplorer.Selection.Item(1)
    Set doClipboard = CreateObject(DATAOBJECT_BINDING)
   
    If objItem.Class = olMail Then
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][MAIL: " & OPSanitise(objItem.Subject) & " (" & OPSanitise(objItem.SenderName) & ")]]"
        
    ElseIf objItem.Class = olAppointment And objItem.MeetingStatus <> olNonMeeting Then ' Regular meetings
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][MEETING: " & OPSanitise(objItem.Subject) & " (" & OPSanitise(objItem.Organizer) & ")]]"
        
    ElseIf objItem.Class = olAppointment And objItem.MeetingStatus = olNonMeeting Then ' Handle appointments differently - remove organiser/attendees
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][APPT: " & OPSanitise(objItem.Subject) & "]]"
        
    ElseIf objItem.Class = olTask Then
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][TASK: " & OPSanitise(objItem.Subject) & " (" & OPSanitise(objItem.Owner) & ")]]"
        
    ElseIf objItem.Class = olContact Then
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][CONTACT: " & OPSanitise(objItem.Subject) & " (" & OPSanitise(objItem.FullName) & ")]]"
        
    ElseIf objItem.Class = olJournal Then
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][JOURNAL: " & OPSanitise(objItem.Subject) & " (" & objItem.Type & ")]]"
        
    ElseIf objItem.Class = olNote Then
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][NOTE: " & OPSanitise(objItem.Subject) & "]]"
        
    Else
        doClipboard.SetText "[[outlook:" & objItem.EntryID & "][ITEM: " & OPSanitise(objItem.Subject) & " (" & OPSanitise(objItem.MessageClass) & ")]]"
    End If
    
    doClipboard.PutInClipboard
    
    Set objItem = Nothing
    Set doClipboard = Nothing
   
End Sub