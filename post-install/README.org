#+TITLE: Post-install instructions

Instructions for what to do with files in the ~post-install~ folder.

* Registry Entries

This folder contains a number of ~.reg~ files which should be applied after
installation (i.e. once the archive has been extracted).

~emacs-context-menus.reg~ will add 'Edit with Emacs' or 'Open with Dired' to
context menus on windows.

The remaining ~.reg~ files are programatically generated during the build
process & either set environment variables or generate startup commands, you
will need to restart for these to take effect

* org-mode & Outlook integration

- ~OrgMode.bas~ contains VBA scripts used for integration between Microsoft
Outlook and org-mode. To install, in Outlook, first add the developer tab to the
ribbon, then use it to open the visual basic editor. From there you can use the
File menu to import the ~.bas~ file.

- ~org-mode-outlook-ribbon.exportedUI~ will create an org-mode group in the
  ~Main~ tab of each view in Outlook (Mail, Calendar, Contacts etc.), as well as
  the Main tab for opened emails & in the ~Quick Access~ menu globally. The
  buttons it creates are linked to the scripts imported from ~OrgMode.bas~ To
  install, right click on the ribbon in any view, choose ~Customize...~ then use
  the Import button to select the ~.exportedUI~ file.
