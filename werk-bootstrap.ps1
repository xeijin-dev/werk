### --- NOTE: If you are reading from the PS1 script you will find documentation sparse, the --- ###
### --- script is accompanied by an org-mode file, which is used to literately generate it.  --- ###
### --- Please see https://gitlab.com/xeijin-dev/propositum for the accompanying README.org. --- ###

# set default error action pref to stop the AppVeyor build when we hit errors
# $ErrorActionPreference = "Stop" # getting stuck on DOOM commands in config

# initiate the .NET stopwatch class so we can time different parts of the script
$ScriptTotalTime = [system.diagnostics.stopwatch]::StartNew()
$StopWatch = [system.diagnostics.stopwatch]::StartNew()

# import helper functions
. "$PSScriptRoot/libexec/werk-helper-fns.ps1"
$env:Path = "C:\Program Files\Git\bin;$env:Path" # always use git bash



Write-Host -ForegroundColor White -BackgroundColor DarkCyan "`n  >>>>>  `n    💼 werk | bootstrap >>> `n`n this script is designed to work both locally and via ci/cd (AppVeyor) `n ensure you set the '`$env:werkConfig' environment variable to the `n location of the YAML config prior to running the script.  `n`n  see https://gitlab.com/xeijin-dev/werk for more info  `n  => `n"

## -- Initialisation -- ##

Write-Step "Initialising ... Please wait."
Set-Location $PSScriptRoot

# enable script execution & tls/ssl
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"

# add powershell modules
$psModules = @('powershell-yaml',
               'ScoopPlaybook',
               'PSMarkdown')

foreach ($module in $psModules) {
   if (-not (Get-Module -ListAvailable -Name $module)) {
      Install-Module $module -Scope CurrentUser
   }
}

# everything that needs to be edited is stored in a yaml config file, the path to which is defined in '$env:werkConfig'
# To get exception types, in catch block: Write-Host $Error[4].Exception.GetType().FullName
try {
   if (-not (Test-Path -Path $env:werkConfig)) {
      Write-Step -AsError "`nThe path '$env:werkConfig' defined in '`$env:werkConfig' is invalid.`n"
      throw
   }
}
catch [System.Management.Automation.ParameterBindingException] {
   Write-Step -AsError "No config path defined in '`$env:werkConfig`', falling back to default."
   $env:werkConfig = "$PSScriptRoot/.install/werk-config.yml"
}
if (Test-Path -Path $env:werkConfig){
   Write-Step -AsSuccess "Found config: '$env:werkConfig'"
}
else {Write-Step -AsError "No valid config was found."; throw}

# parse data from config.yaml into a PSObject
$config = Get-Content $env:werkConfig | Out-String | ConvertFrom-Yaml -AllDocuments -Ordered

# since we're preserving the ansible playbook format (e.g. win_regedit)
# make the variable names a bit less wieldy to work with
$scoopBuckets = $config.scoop.buckets
$scoopApps = $config.scoop.apps 
$directories = $config.directories.win_file

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"

Write-Step "Setting environment variables & registry values ..."

#$regItems = $config.registry.Keys.GetEnumerator() | % {$_ | -join() | Out-String -Stream |  } # get the key name as a string

$regExport = @()
# walk through registry items and set each one, evaluating any environment vars for current session
for ($r = 0; $r -lt $config.registry.count; $r++) {
   
   foreach ($entry in $config.registry[$r].win_regedit) {

      # create keys that don't exist
      if (-not (Test-Path -Path $entry.path)) {
         # because of a strange bug can't use new-item as it will say key already exists, need to invoke .NET registry class instead...
         ([Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::CurrentUser, $env:COMPUTERNAME)).CreateSubKey($entry.path.Replace('HKCU:\',''))

         # '(Default)' values of a registry key are special, handle them separately
         if ($entry.type -eq 'Default') {
            Set-Item -Path $entry.path -Value $entry.data -Force 
         }
      else {Write-Host "Key exists."}
      }

      # otherwise persist the value in the registry & overwrite any existing value with -Force
      if (Test-Path -Path $entry.path) {

         # '(Default)' values of a registry key are special, handle them separately
         if ($entry.type -eq 'Default') {
            Set-Item -Path $entry.path -Value $entry.data -Force 
         }
         else {New-ItemProperty -Path $entry.path -Name $entry.name -Value $entry.data -PropertyType $entry.type -Force}
      }

      # further, if it's an environment variable, make it available for the current powershell session *with* expanded paths
      if ($entry.path -eq 'HKCU:\Environment') {
         $expandedRegistryVar = Get-UserEnvVar -ExpandVars -EnvName $entry.name 
         Set-Item "env:$($entry.name)" -Value $expandedRegistryVar
      }

      # create the list of keys to export to export, add each key as a hash table to an existing array in order to allow duplicate keys (e.g. the '(Default)' keys)
      # the array can be returned in a 'list' format that is very similar to a hash table (i.e. Name & Value)
      $regExport += @{$entry.name = $entry.path}

   }

}

# refresh user env vars so that they're available to other applications
Refresh-EnvironmentVariables

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Mapping root drive ..."
# map the root folder to the specified drive letter (for AppVeyor, always use the build folder)
$propositumRealPath = if ($env:AppVeyor) {$env:APPVEYOR_BUILD_FOLDER} else {$env:propositumRealPath}
Write-Step -Sub "$env:propositumDrive -> $env:propositumRealPath"
subst $env:propositumDrive $propositumRealPath

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Creating directories ..."

# create required directories
Push-Location $env:propositumRoot # paths can be relative to root, so head there first
for ($i = 0; $i -lt $directories.count; $i++) {

   # create using function (which checks for existing first)
   Create-DirectoryIfNotExist $directories[$i].path

}
Pop-Location

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
# extract fonts to the fonts directory
Write-Step "Extracting fonts ..."

Push-Location './other'
"7z x -bsp1 {0}\other\fonts.7z -o{1}\fonts" -f $env:APPVEYOR_BUILD_FOLDER,$env:propositumRoot | Iex-Native
Remove-Item "fonts.7z" -Force
Pop-Location

# copy home directory
# eventually want to replace this with stow or git submodule for dotfiles
Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Copying 'home' directory ..."
Push-Location './home'
'robocopy ./ {0} /E /is' -f $env:propositumHome | iex
#Copy-item -Force -Recurse './*' -Destination $env:propositumHome
Pop-Location

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Exporting registry entries for post-install ..."
# transpose the the array of registry keys to export into a dictionary of unique keys (paths) and associated values
$regTemp = 'C:\temp\werk-reg-export'
$regAdd = "$env:propositumDrive/post-install/propositum-registry-add.reg"
$regDel = "$env:propositumDrive/post-install/propositum-registry-del.reg"
$uniqueRegKeys = $regExport.Values | Select -Unique
$keys = [ordered]@{}
foreach ($keyPath in $uniqueRegKeys) {
   $uniqueRegVals = @()
   $uniqueRegVals = $regExport | Where-Object { $_.Values -in $keyPath } | Select Keys -unique -ExpandProperty Keys
   $keys.Add($keyPath, $uniqueRegVals)
}
foreach ($entry in $keys.Keys) {
    Export-RegistryValues -Path $entry -RegistryValues $keys[$entry] -ExportPath $regTemp
}
# adapted from: https://stackoverflow.com/a/28077331
# $keys | % {
#   $i++
#   & reg export $_ "$tempFolder\$i.reg"
# }
Write-Step -Sub "Make registry 'add' file ..."
'Windows Registry Editor Version 5.00' | Set-Content $regAdd
Get-Content "$regTemp\*.reg" | ? {
  $_ -ne 'Windows Registry Editor Version 5.00'
} | Add-Content $regAdd
Write-Step -Sub -AsSuccess $regAdd
Write-Step -Sub "Make registry 'delete' file ..."
If ($regAdd | Test-Path) { Get-Content $regAdd | % {
                               $_ -replace '(\"\w+\"|\@)(\=.*)', '$1=-' `
                                  -replace '^(\s+.[^\[\"]*)', '' } | Set-Content $regDel }
Write-Step -Sub -AsSuccess $regDel

## -- Installation -- ##

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Preparing for installation ..."
# download and install scoop if we don't already have it

Invoke-Expression (new-object net.webclient).downloadstring('https://get.scoop.sh')

## NOT WORKING
# try {
#    Get-Command 'scoop' 
# }

# catch [System.Management.Automation.CommandNotFoundException] {
#    Write-Step -Sub -AsError "Scoop installation not found. `n Installing..."
#    Invoke-Expression (new-object net.webclient).downloadstring('https://get.scoop.sh')
#  }
iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
# add scoop buckets
$scoopBuckets | % { "scoop bucket add {0} {1}" -f [string]$_.keys,[string]$_.values | iex } # includes 'main' bucket which scoop will complain already exists, but needed the full list defined for getting manifests later on

# update scoop
$scoopApps.values | ? { $_.version -eq 'nightly' } | select scoopName | % { "scoop cache rm {0}" -f [string]$_.scoopName } # modules that have a version set to 'nightly' should have cache cleared in-case we make changes in the same day
scoop update 

# copy over the `scoop-dlcache` script
# TODO: fix dlcache script, broken by latest scoop updates
Copy-Item -Path "$PSScriptRoot\libexec\scoop-dlcache.ps1" -Destination "$env:propositumDrive\apps\scoop\current\libexec"

# get info for each app from manifests
Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Getting application information ..."
# search all scoop buckets to get a list of paths to each app's .json manifest
$manifestNames = $scoopApps | % { $_ + '.json' }
$manifests = @()
foreach ($bucket in $scoopBuckets.Keys) {

   $manifests += @(Get-ChildItem -Path $env:propositumRoot\buckets\$bucket\bucket\* -Include $manifestNames | Select BaseName,FullName)
}
# create ordered psobject of $scoopApps
for ($i = 0; $i -lt $scoopApps.Count; $i++) {

   $app = $config[0][0][1][$i] # TODO: find this programatically using $($config.keys).indexOf() - currently hard-coded, causes subtle errors if ever shift order or structure of config around
   $manifest = $manifests | ? { $_.BaseName -eq $app }
   $manifestData = Get-Content $manifest.FullName | Out-String | ConvertFrom-Yaml -Ordered
   $manifestData.Add('scoopName',$manifest.BaseName) # Add the component name into the PSObject for convenience

   $config[0][0][1][$i] = @{$manifest.BaseName = [pscustomObject]$manifestData}
}

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Generating list of applications to install ..."

# create a markdown table of the components installed + licenses
$tbl = @()
for ($i = 0; $i -lt $scoopApps.Count; $i++) {

   $tmpObj = [PScustomObject]@{}
   $appPSObj = $scoopApps[$i].Values
   $component = "[" + [string]$scoopApps[$i].Keys + "]" + "(" + $appPSObj.homepage + ")"
   $licenseAndUrl = if ($appPSObj.license.url) {"[" + $appPSObj.license.identifier + "]" + "(" + $appPSObj.license.url + ")"}
   $license = if ($licenseAndUrl) {$licenseAndUrl} else {$appPSObj.license}
   $description = $appPSObj.description
   $categorisation = $appPSObj._comment
   $version = $appPSObj.version

   Add-Member -InputObject $tmpObj -MemberType NoteProperty -Name component -Value $component
   Add-Member -InputObject $tmpObj -MemberType NoteProperty -Name license -Value $license
   Add-Member -InputObject $tmpObj -MemberType NoteProperty -Name usage -Value $description
   Add-Member -InputObject $tmpObj -MemberType NoteProperty -Name categorisation -Value $categorisation
   Add-Member -InputObject $tmpObj -MemberType NoteProperty -Name version -Value $version
   
   $tbl += $tmpObj
}

Write-Step -Sub "Exporting list of components to markdown file ..."

# markdown conversion
$tbl = $tbl | ConvertTo-Markdown
$tbl | Out-File "components.md"
# add a copy into the apps folder alongside install-info.txt (which we generate later)
Push-Location $env:propositumApps
$tbl | Out-File "components.md"
Pop-Location

# relay to the user whilst there's still time to cancel the build...
$componentsToInstall = $scoopApps.Keys -join "`r`n=> " | Out-String

Write-Host "`r`n  The following applications will be installed:  `r`n`r`n=> $componentsToInstall" -ForegroundColor White -BackgroundColor DarkCyan

# install scoop components
Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-step "Installing applications ..."

$scoopApps.keys | % { iex "scoop install $_"; Write-TimeElapsed $StopWatch; $StopWatch.Restart() }

## doom-emacs requires special handling given some specific dependencies that can't be resolved pre install
Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Install doom-emacs ..."
Write-Step -Sub "pre-registering fonts ..."
"ruf $env:propositumDrive/fonts" | iex # so that later doom-install commands do not error-out
Write-Step -Sub "git clone .doom.d & .emacs.d ..." # for syncing changes made locally/at work with future updates
'git clone --verbose --progress https://gitlab.com/xeijin-dev/doom-config.git {0}' -f $env:DOOMDIR | Iex-Native
'git clone --verbose --progress https://github.com/hlissner/doom-emacs {0}' -f $env:EMACSDIR | Iex-Native
Write-Step -Sub "find git bash ..."
# find git's bash.exe ---- must use the git version of bash.exe as WSL bash doesn't mount subst'd drives & doesn't maintain windows path & variables (so emacs.exe isn't found)
switch ("$env:propositumApps/cmder-full/current/vendor/git-for-windows/bin/bash.exe",
        "$env:propositumApps/git-with-openssh/current/bin/bash.exe",
        "$env:propositumApps/git/current/bin/bash.exe",
        'bash')
{
    {$_ | Test-Path} {$GitBash = $_; Write-Step -Sub -AsSuccess $GitBash; Break}
    {$_ | Get-Command -ErrorAction ignore} {$GitBash = $_; Write-Step -Sub -AsSuccess $GitBash; Break}
    default {Write-Step -Sub -AsError "git-bash could not be found for '$_'.`ngit-bash is required to correctly install doom"}
}
Write-Step -Sub "run doom install ..."
# execute doom install
## note --no-fonts and --no-env don't appear to work via AppVeyor, but automatic font install disabled for windows by default anyway, can live with no-env
'{0} "{1}bin/doom" -y install --no-fonts --no-env' -f $GitBash,$env:EMACSDIR | Iex-Native

# create additional shortcuts and symlinks
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Creating shortcuts & symlinks ..."

# if (-not (Get-Command 'sudo')) {
#    if (-not (Test-Administrator)) {
#       Write-Step -Sub -AsError "`nYou did not install 'sudo' or you are not running this script as an administrator, some operations may fail.`n"
#    }
# }

Push-Location $env:propositumRoot
foreach ($sc in $config.shortcuts) {

   $type = [string]$sc.Keys
   $start = Invoke-Expression "$sc[$type].start"
   $name = [string]$sc[$type].name
   $value = [string]$sc[$type].value
   $path = [string]$sc[$type].path
   # $sudo = if (Get-Command -ErrorAction SilentlyContinue 'sudo') {'sudo'}
   $itemExists = if ($start) {Push-Location ($start); Test-Path -Path $path; Pop-Location} else {Test-Path -Path $path}
   $createItem = 'if ($start) {Push-Location ($start); New-Item -Force -ItemType $type -Value $value -Path $path; Pop-Location'

   if ($itemExists) {Write-Step -Sub -AsError "[$type] `"$name`" already exists, overwriting."}

   try {Invoke-Expression $createItem | Out-Null; Write-Step -Sub -AsSuccess "[$type] Created `"$name`"."}

   catch {Write-Step -Sub -AsError "[$type] Failed to create `"$name`"."}

}
Pop-Location

Write-TimeElapsed $StopWatch
$StopWatch.Restart()
Write-Host "`n"
Write-Step "Cleaning up ..."
# remove the cache so we save some space on the build artifact
scoop cache rm *

# confirm what has been installed & keep a record for future ref.
Write-Host "`n"
Write-Step "Generating install metadata ..."
scoop list | Write-Host
Push-Location $env:propositumInstall
Write-Step -Sub "scoop installed packages file ..."
scoop export | Out-String > scoop-installed-packages.txt
Get-ChildItem "./" -Filter "scoop-installed-packages.txt" | %{ Write-Step $_.FullName -Sub -AsSuccess }
Write-Step -Sub "werk commit ref ..."
$env:APPVEYOR_REPO_COMMIT | Out-String > werk-installed-commit.txt # add the current werk commit to install-info.txt
Get-ChildItem "./" -Filter "werk-installed-commit.txt" | %{ Write-Step $_.FullName -Sub -AsSuccess }
Write-Step -Sub "emacs straight.el version lockfile ..."
emacs --batch -l $env:EMACSDIR/init.el -l $env:EMACSDIR/.local/autoloads.pkg.el -l straight --eval --% "(progn (doom-initialize) (let ((prefix-arg 4)) (straight-freeze-versions)))"
# emacs --batch -l $env:EMACSDIR/init.el -l $env:EMACSDIR/.local/autoloads.pkg.el -l straight --eval --% "(let ((prefix-arg 4)) (doom-initialize) (straight-freeze-versions))" # create lockfile for elisp packages
robocopy "$env:EMACSDIR\.local\straight\versions" $env:propositumInstall "default.el" # copy the lockfile to the $env:propositumInstall
Get-ChildItem "./" -Filter "default.el" | %{ Write-Step $_.FullName -Sub -AsSuccess }
Write-TimeElapsed $StopWatch
Pop-Location

# $blockRdp = $true; iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
## -- Create Build Artifact (AppVeyor only) -- ##

# create the build artifact on ci/appveyor only
if ($env:AppVeyor)
{
    Write-Step -Sub -AsSuccess "Detected AppVeyor or CI build ..."
    
    Write-TimeElapsed $StopWatch
    $StopWatch.Restart()
    Write-Step "Preparing release artifact, this may take some time ..."

    $artifactName = 'werk' + '-' + (Get-Date -format 'yyyy-MMM-dd') + '-' + -join ($env:APPVEYOR_REPO_COMMIT.ToCharArray() | Select-Object -First 8) # give it a unique name with the date + first 5 letters of commit

    Write-Step -Sub "Compressing files into release artifact..."
    Set-Location $env:propositumRoot # cd to root, as 7z -v switch does not support specifying end file and directory
    
    Write-Step -Sub "Creating TAR archive..."
    "7z a -bsp1 -ttar -snl {0}.tar ./" -f $artifactName | Iex-Native  # Create tar archive to preserve symlinks
    
    Write-TimeElapsed $StopWatch
    $StopWatch.Restart()
    Write-Step -Sub "Compressing TAR into 7z archive..."
    "7z a -bsp1 -t7z {0}.tar.7z {0}.tar -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on -v1500m" -f $artifactName | Iex-Native # Compress tar into 7z archive
    
    if ($env:bintrayDeploy) {
    # Workaround for AppVeyor BinTray issue (only accepts .zip archives)
    # TODO: BinTray only accepts 250mb max per file for free/OSS accounts causing 'text/plain' error when appveyor tries to upload to bintray
    Write-TimeElapsed $StopWatch
    $StopWatch.Restart()
    Write-Step -Sub "Create zip archive for BinTray deployment..."
     "7z a -bsp1 -tzip {0}.zip {0}*" -f $artifactName | Iex-Native
    }
    
    # appveyor debug
    #$blockRdp = $true; iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
    
   }

Write-TimeElapsed $StopWatch

$StopWatch.Stop()
Write-Step -AsSuccess "Build complete!"
$scriptTotalMsg = "Total Time Taken: {0}" -f (Get-Time -Seconds $ScriptTotalTime.Elapsed.Seconds)
$ScriptTotalTime.Stop()
Write-Step -AsSuccess $scriptTotalMsg
