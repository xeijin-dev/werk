
# Table of Contents

1.  [Usage](#orgd851514):README:
2.  [Define](#orge0fa782)
    1.  [Components](#orgcf0d9c8):README:
    2.  [Variables](#orgef298fb):README:



<a id="orgd851514"></a>

# Usage     :README:

Installation via the [Releases](https://gitlab.com/xeijin/propositum/releases) for the latest binary distribution. Unzip, then ensure you run
the post-install ps1 script.

    ### --- NOTE: If you are reading from the PS1 script you will find documentation sparse, the --- ###
    ### --- script is accompanied by an org-mode file, which is used to literately generate it.  --- ###
    ### --- Please see https://gitlab.com/xeijin-dev/propositum for the accompanying README.org. --- ###


<a id="orge0fa782"></a>

# Define

Define key [2.1](#orgcf0d9c8) and [2.2](#orgef298fb)


<a id="orgcf0d9c8"></a>

## Components     :README:

Next, within the table, define the environment variables and their desired values

    usage                                                                              | version      | categorisation                      | component                                                            | license                                                             
    ---------------------------------------------------------------------------------- | ------------ | ----------------------------------- | -------------------------------------------------------------------- | --------------------------------------------------------------------
    Portable console emulator for Windows                                              | 1.3.11       |                                     | [cmder-full](http://cmder.net)                                       | MIT                                                                 
                                                                                       | 26.2         |                                     | [emacs-p](https://www.gnu.org/software/emacs/)                       | GPL-3.0                                                             
    personal '.doom.d' for use with doom-emacs-p                                       | nightly      | Categorisation: Configuration files | [doom-config-p](https://gitlab.com/xeijin-dev/doom-config)           | [MIT](https://gitlab.com/xeijin-dev/doom-config/blob/master/LICENSE)
                                                                                       | nightly      |                                     | [doom-emacs-p](https://github.com/hlissner/doom-emacs)               | MIT                                                                 
    Usage: make text fields and inputs editable with emacs and other text editors      | 2.01         | Categorisation: Standalone tool     | [texteditoranywhere-p](https://www.listary.com/text-editor-anywhere) | Freeware                                                            
                                                                                       | 2.7.3        |                                     | [pandoc](https://pandoc.org/)                                        | GPL-2.0-or-later                                                    
                                                                                       | 2.38         |                                     | [graphviz](https://www.graphviz.org/)                                | EPL-1.0                                                             
                                                                                       | 1.2019.6     |                                     | [plantuml](http://plantuml.com/)                                     | GPL-3.0-only                                                        
                                                                                       | 7.3.0        |                                     | [fd](https://github.com/sharkdp/fd)                                  | MIT                                                                 
                                                                                       | 0.18.0       |                                     | [fzf](https://github.com/junegunn/fzf)                               | MIT                                                                 
                                                                                       | 11.0.1       |                                     | [ripgrep](https://github.com/BurntSushi/ripgrep)                     | MIT                                                                 
    An approximation of the Unix sudo command. Shows a UAC popup window unfortunately. | 0.2018.07.25 |                                     | [sudo](https://github.com/lukesampson/psutils)                       | MIT                                                                 


<a id="orgef298fb"></a>

## Variables     :README:

Define the environment variables and their desired values in the table

    ---
    scoop:
    
      buckets: # bucketName: url
    
        - main:
        - extras:
        - propositum: https://gitlab.com/xeijin-dev/propositum-bucket
    
      apps:
    
        - cmder-full
        # - lunacy
        # - autohotkey
        # - miniconda3
        # - imagemagick
        # - inkscape
        # - knime-p
        # - rawgraphs-p
        # - regfont-p
        - emacs-p
        - doom-config-p
        - doom-emacs-p
        - texteditoranywhere-p
        # - superset-p
        - pandoc
        # - latex
        - graphviz
        - plantuml
        # - draw-io-p
        # - greenshot
        - fd
        - fzf
        - ripgrep
        - sudo
    ...
    ---
    registry:
    
      vars: # user environment variables which will be persisted
    
        - name: Propositum 'Real Path'
          win_regedit:
            path: HKCU:\Environment
            name: propositumRealPath
            data: 'C:/propositum'
            type: String
    
        - name: Propositum Mapped Drive
          win_regedit:
            path: HKCU:\Environment
            name: propositumDrive
            data: 'P:'
            type: String        
    
        - name: Propositum 'root'
          win_regedit:
            path: HKCU:\Environment
            name: propositumRoot
            data: '%propositumDrive%\'
            type: ExpandString
    
        - name: scoop base directory
          win_regedit:
            path: HKCU:\Environment
            name: SCOOP
            data: '%propositumRoot%'
            type: ExpandString
    
        - name: Propositum 'home' folder
          win_regedit:
            path: HKCU:\Environment
            name: propositumHome
            data: '%propositumDrive%\home'
            type: ExpandString
    
        - name: Propositum 'apps' folder
          win_regedit: 
            path: HKCU:\Environment
            name: propositumApps
            data: '%propositumDrive%\apps'
            type: ExpandString
    
        - name: graphviz dot.exe binary location
          win_regedit:
            path: HKCU:\Environment
            name: GRAPHVIZ_DOT
            data: '%propositumApps%\graphviz\current\bin\dot.exe'
            type: ExpandString
    ...
    ---
    shortcuts: 
    # Link type options (from New-Item): File | Directory | SymbolicLink | Junction | HardLink
    
      - SymbolicLink: 
          name: 'doom-config-p symlink home'
          start: $env:propositumApps
          value: ./doom-config-p/current/
          path: ../home/.doom.d
    
      # - SymbolicLink: 
      #     name: 'emacs-p symlink home'
      #     start: $env:propositumApps
      #     value: ./emacs-p/current/
      #     path: ../home/emacs-p
    
      - SymbolicLink: 
          name: 'doom-emacs-p symlink home'
          start: $env:propositumApps
          value: ./doom-emacs-p/current/
          path: ../home/.emacs.d
    ...
    # ---
    # vars:
    
    #   core: 
    #   # core variables that must be specified by the user
    
    #     - &realPath "C:/propositum"
    #     - &rootDrv "P:" # this will be the drive '&realpath' is mapped to
    
    #   env: 
    #   # variables that can be safely set globally for the user (for windows, these are set via the registry)
    
    #     propositum_realPath: *realPath
    
    #     propositum_root:
    #       - *rootDrv
    #       - /
    
    #     propositum_apps:
    #       - *rootDrv
    #       - /apps
    
    #     propositum_home: &home
    #       - *rootDrv
    #       - /home
    
    #     SCOOP: 
    #      - *rootDrv
    #      - /
    
    #     GRAPHVIZ_DOT:
    #       - *rootDrv
    #       - /apps
    #       - /graphviz/current/bin/dot.exe
    
    #   session:
    #   # variables that should only be set per session, or locally in scripts
    
    #     HOME: *home
    # ...

