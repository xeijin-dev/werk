;; assist helper (xeijin - 2019)

;;; note - this script purposely relies on #NoEnv *not* being set to allow use of environment variables without having to explicitly register them

;; setup tray icon
;FileInstall, feather-icons.dll, %A_ScriptDir% ; embeds the feather-icons.dll into the script upon compile
global featherIcons := A_ScriptDir "\feather-icons.dll"
global assistIcon := A_ScriptDir "\assist.ico" ; explicit icon file path (prevents issues finding icon file when executing the script from other directory)
menuTitle := ">> assist-helper" ; get version from registry or file?

; Create the tray icon & menu
#Persistent
#SingleInstance
Menu, Tray, Color, FF7F50
Menu, Tray, Tip, assist-helper
Menu, Tray, Click, 2
Menu, Tray, Icon, %assistIcon%
Menu, Tray, NoStandard

;; define config location / environment variables & define global variables

; default location of ini file (same directory as executable)
global iniFile := A_ScriptDir "\assist-config.ini"
global emacsInstancePids := [] ; initialise to track process ids for instances
global emacsDaemonPid ; initialise to track process id for daemon

;; define helper functions

ChangeWindowIcon(pid, icon) {
  ;change icon of window to custom icon - @SKAN: https://autohotkey.com/board/topic/19781-can-ahk-change-the-icon-of-a-window/
  
    Winwait, ahk_pid %pid%
    ID := WinExist("ahk_pid" pid)
    
    hIcon := DllCall( "LoadImage", UInt,0, Str,icon, UInt,1, UInt,0, UInt,0, UInt,0x10 )
    SendMessage, 0x80, 0, hIcon ,, ahk_id %ID%  ; One affects Title bar and
    SendMessage, 0x80, 1, hIcon ,, ahk_id %ID%  ; the other the ALT+TAB menu
    
    return ErrorLevel
}

; stdout of process to variable - @cyruz: https://www.autohotkey.com/boards/viewtopic.php?t=791
StdoutToVar_CreateProcess(sCmd, sEncoding:="CP0", sDir:="", ByRef nExitCode:=0) {
    DllCall( "CreatePipe",           PtrP,hStdOutRd, PtrP,hStdOutWr, Ptr,0, UInt,0 )
    DllCall( "SetHandleInformation", Ptr,hStdOutWr, UInt,1, UInt,1                 )

            VarSetCapacity( pi, (A_PtrSize == 4) ? 16 : 24,  0 )
    siSz := VarSetCapacity( si, (A_PtrSize == 4) ? 68 : 104, 0 )
    NumPut( siSz,      si,  0,                          "UInt" )
    NumPut( 0x100,     si,  (A_PtrSize == 4) ? 44 : 60, "UInt" )
    NumPut( hStdOutWr, si,  (A_PtrSize == 4) ? 60 : 88, "Ptr"  )
    NumPut( hStdOutWr, si,  (A_PtrSize == 4) ? 64 : 96, "Ptr"  )

    If ( !DllCall( "CreateProcess", Ptr,0, Ptr,&sCmd, Ptr,0, Ptr,0, Int,True, UInt,0x08000000
                                  , Ptr,0, Ptr,sDir?&sDir:0, Ptr,&si, Ptr,&pi ) )
        Return ""
      , DllCall( "CloseHandle", Ptr,hStdOutWr )
      , DllCall( "CloseHandle", Ptr,hStdOutRd )

    DllCall( "CloseHandle", Ptr,hStdOutWr ) ; The write pipe must be closed before reading the stdout.
    While ( 1 )
    { ; Before reading, we check if the pipe has been written to, so we avoid freezings.
        If ( !DllCall( "PeekNamedPipe", Ptr,hStdOutRd, Ptr,0, UInt,0, Ptr,0, UIntP,nTot, Ptr,0 ) )
            Break
        If ( !nTot )
        { ; If the pipe buffer is empty, sleep and continue checking.
            Sleep, 100
            Continue
        } ; Pipe buffer is not empty, so we can read it.
        VarSetCapacity(sTemp, nTot+1)
        DllCall( "ReadFile", Ptr,hStdOutRd, Ptr,&sTemp, UInt,nTot, PtrP,nSize, Ptr,0 )
        sOutput .= StrGet(&sTemp, nSize, sEncoding)
    }
    
    ; * SKAN has managed the exit code through SetLastError.
    DllCall( "GetExitCodeProcess", Ptr,NumGet(pi,0), UIntP,nExitCode )
    DllCall( "CloseHandle",        Ptr,NumGet(pi,0)                  )
    DllCall( "CloseHandle",        Ptr,NumGet(pi,A_PtrSize)          )
    DllCall( "CloseHandle",        Ptr,hStdOutRd                     )
    Return sOutput
}

ExecutableFind(executable, dirOnly:=False) {

    executable:= StdoutToVar_CreateProcess("where " executable)
    executable := RegExReplace(executable, "`n")

    ; find real path if shim
    executablePath := GetShimRealPath(executable)

    ; truncate the executable name if requested
    if (dirOnly) { 
        SplitPath, executablePath, , executablePath
    }

    return executablePath    
}

GetShimRealPath(shimPath) {

        ; manual handling for scoop shims - using the original executable is significantly faster than using the shim
        if inStr(shimPath, "shims")
        {
            SplitPath, shimPath, , shimDir, , shimName ; get the shim name & parent directory
            FileRead, realPath, % shimDir "/" shimName ".shim"
            realPath := RegExReplace(realPath, "path = ") ; extract path from .shim file
        }
        else {
            realPath = shimPath ; otherwise not a shim, return path as-is
        }

        return realPath
}

; retrieve location of emacs.exe and, if we can find it, set emacs_dir
GetEmacsDir(setEnv:=false){

    EnvGet, existingEnv, emacs_dir

    if !(existingEnv) { 
        ; look for emacs.exe on path & handle if it's a scoop shim
        emacsExecDir := ExecutableFind("emacs", True)

        ; extract emacs_dir path & populate variable
        SplitPath, emacsExecDir, , emacs_dir
        
        if (setEnv) {
            EnvSet, emacs_dir, % emacs_dir
        }
        
    }
    else {
        if (setEnv) {
            EnvSet, emacs_dir, % existingEnv
        }
        else {
            emacs_dir = % existingEnv
        }
    }

    return emacs_dir ; just in case the shim exe or actual path is ever needed
}


ParseVars(varArray) {
    ; parses vars from an array of strings in "var=val" format

    ; initialse assoc array for envVar names as return value for other functions
    varAssocArray := {}

    ; loop through and set as environment variables
    For v in varArray
    {
        ; use regexp with two match groups to get vars as var1 and var2 respectively
        RegExMatch(varArray[A_Index], "(\S+)\s?=\s?(.*)", var)
        
        ; deref to resolve nested environment variables
        Transform, varDeref, deref, %var2% 
        
        ; push values into assoc array
        varAssocArray[(var1)] := (varDeref)
    }

    ; return assoc array of var names & values
    return varAssocArray
}

SetVars(varAssocArray, envVar:=false) {
    ; takes an associative array {key:value,} of vars and sets as either script or environment variables

    for var, val in varAssocArray {
    
        if (envVar) { ; to persist for other programs that are launched
            EnvSet, %var%, %val%
        }
        else { ; set as script var, NOT persisted to other programs that are launched
            %var% = %val%
        }
    }
    
   return
}

LoadSettingsFromConfig(iniSection, configFile) {
    ; loads settings from the given config file (.ini), default is to use the iniFile variable

    if FileExist(configFile) {
    
        ; read EnvironmentVariables section into a multi-line string var
        IniRead, iniLoad, %configFile%, %iniSection%
        
        if !(iniLoad) {
            Throw "no entries could be read from the ini file:`n" configFile
        }
    
        ; convert multi-line string to array (strip spaces from beginning and end) 
        iniVars := StrSplit(iniLoad, "`n", " `t")
        
        ; set vars as defined in the ini file
        SetVars(ParseVars(iniVars), true)
    
    }
    else {
        AssistToast("no config file found, generating config")
        GenerateConfig(configFile)
        LoadSettingsFromConfig(iniSection, configFile)
    }
    
    return
}

; generate ini with information found 
GenerateConfig(file) {

    if FileExist(file) {
        Throw "config file already exists:`n" file
    }

    ; determine and set emacs_dir Envvar first, if not already defined
    GetEmacsDir(true)
    
    ; define defaults values for key environment variables
    defaultEnvVars := ["emacs_dir=%emacs_dir%", "HOME=%USERPROFILE%", "EMACSDIR=%HOME%\.emacs.d", "EMACS_SERVER_FILE=%EMACSDIR%\.local\cache\server\server"]
    
    parsedEnvVars := ParseVars(defaultEnvVars)

    ; loop through the varList and write each entry to ini file
    For parsedVar, parsedVal in parsedEnvVars
    {
        EnvGet, existingEnv, %parsedVar%
        
        if (existingEnv) { ; if an environment variable already exists write that to the ini
        
            IniWrite, %existingEnv%, %file%, EnvironmentVariables, %parsedVar%
        }
        else { ; otherwise take the default and write to ini
            IniWrite, %parsedVal%, %file%, EnvironmentVariables, %parsedVar%
        }
    }

    return file
}

;GenerateConfig(iniFile)

AssistLoadSettings() {
    ; wrapper for LoadSettingsFromConfig, passing in default iniFile location
    
    LoadSettingsFromConfig("EnvironmentVariables", iniFile)
    
    ; use sub-routine to load global vars (doing so in function was causing issues)
    GoSub, GetExecutablePaths
    
    return
    
}

EditConfig() {
; open settings file for editing (in emacs if available, otherwise using default editor)

    if (GetDaemonStatus() = "running") {
        Run, %emacsclientw% -c -a 'runemacs' iniFile, %HOME%
    }
    else {
            Run, Edit %iniFile%
    }
    
    return
}

; desktop toast notification
AssistToast(message) {
    TrayTip, assist, %message%, , 16
}

; check if process exists (pre-pend with '!' for 'not')
ProcessExist(Name) {
    if Name 
    {
        Process, Exist, %Name%
        return Errorlevel
    }
    else 
    {
        return
    }
}

LaunchInstance() {
    ; launch a new instance of emacs (non-daemon)

    Run, %runemacs%, %HOME%, currentEmacsInstancePid

    emacsInstancePid.Push(emacsInstancePid) ; add pid to array
    IniWrite, %currentEmacsInstancePid%, %iniFile%, SessionVariables, lastEmacsInstancePid ; capture last daemon Pid within ini file
}

; daemon control functions
StartDaemon()
{

    UpdateDaemonHealth(GetDaemonStatus())

    if !ProcessExist(emacsDaemonPid)
    {
        AssistToast("starting emacs daemon")
        if FileExist(EMACS_SERVER_FILE) 
        {
            FileDelete, %EMACS_SERVER_FILE% ; delete any existing server file if found
        }
        
        ; note that global variables declared in outside function mean no '%' required around vars in run command/target
        Run, %emacs% --daemon, %HOME%, Hide, emacsDaemonPid
        WinWait ahk_pid %emacsDaemonPid%
        ChangeWindowIcon(emacsDaemonPid, assistIcon) ; 
        ;AssistToast("emacs daemon finished loading")

        emacsDaemonPid = %emacsDaemonPid%
        IniWrite, %emacsDaemonPid%, %iniFile%, SessionVariables, lastEmacsDaemonPid ; capture last daemon Pid within ini file

    }

    UpdateDaemonHealth(GetDaemonStatus())
}

QuitDaemon(cmd) { ; default is to quit & prompt to save open buffers

    UpdateDaemonHealth(GetDaemonStatus())

    if ProcessExist(emacsDaemonPid) ; no %% around variables when passing to functions!
    {
        
        ChangeWindowIcon(emacsDaemonPid, assistIcon)
            
        if (cmd = "saveQuit") ; send quit command, prompting for unsaved buffers
        {   
            ;RunWait, %emacsclientw% -c -e "(save-buffers-kill-emacs)"
            
            ; create an invisible frame, then run a command to format it as a mini-buffer only command & allow it to disappear on click
            Run, %emacsclientw% -c -F "((visibility . nil))" -e "(let ((quit-modal (make-frame '((auto-raise . t) (background-color . \"#fa8072\") (foreground-color . \"#000000\") (cursor-color . \"#000000\") (font . \"SF Mono 20\") (height . 5) (internal-border-width . 5) (left . 0.33) (left-fringe . 0) (line-spacing . 3) (menu-bar-lines . 0) (minibuffer . only) (right-fringe . 0) (tool-bar-lines . 0) (top . 48) (undecorated . t) (unsplittable . t) (vertical-scroll-bars . nil) (width . 50)) ))) (set-face-attribute 'minibuffer-prompt quit-modal :foreground \"black\") (select-frame-set-input-focus quit-modal) (local-set-key [mouse-1] (lambda () (interactive) (delete-frame))) (save-buffers-kill-emacs))"
        }
        else if (cmd = "forceQuit") ; or kill without saving
        {
            MsgBox, 49, Force Quit Emacs Daemon, You will lose any unsaved work, are you sure?
            IfMsgBox, OK
                Run, %emacsclientw% -c -n -F "((visibility . nil))" -e "(kill-emacs)" 
            IfMsgBox, Cancel
                return
        }
        else 
        {
            MsgBox invalid daemon stop method '%cmd%'
            return
        }

        Sleep, 3000 ; wait before checking if the process still exists

        if !ProcessExist(emacsDaemonPid) 
        {
            AssistToast("emacs daemon [" emacsDaemonPid "] successfully exited")
        }
        else 
        {
            AssistToast("there was a problem exiting the daemon (" emacsDaemonPid "). Save all buffers then try 'kill all instances'")
        }
    }
    else 
    {
        if !A_ExitReason ; dont show 'process not found' if daemon isnt running and script is exiting
        {
            AssistToast("emacs daemon process not found")
        }
        Exit
    }

    UpdateDaemonHealth(GetDaemonStatus())
}

KillAllProcess(process) {
    MsgBox, 49, Force Quit %process% instances, All instances of %process% will be force terminated. You will lose any unsaved work.
    IfMsgBox, Cancel
        Return
        
        
    IfMsgBox, OK
        Loop
        {
        prev := ErrorLevel
        Process, Close, %process%
        Process, Exist, %process%
        }
        until !ErrorLevel or (prev = ErrorLevel)
        AssistToast("all instances of '" process "' were terminated")

}

NewFrame() {
    global ;

    if !ProcessExist(emacsDaemonPid) 
    {
        StartDaemon()
    }
    else 
    {
        Run, %emacsclientw% -c -a '', HOME, framePid ; create a new frame, but throw error if no server started (no alternate editor)
        ChangeWindowIcon(emacsDaemonPid, assistIcon)
    }
}

CreateDaemonMenu(daemonMenu) {

    ; create submenu items
    Menu, daemonSubMenu, Add, Start daemon, StartDaemon
    Menu, daemonSubMenu, Icon, Start daemon, %featherIcons%, 188, 24

    Menu, daemonSubMenu, Add, Quit daemon, QuitDaemon
    Menu, daemonSubMenu, Icon, Quit daemon, %featherIcons%, 161, 24

    Menu, daemonSubMenu, Add, Force quit daemon, ForceQuitDaemon
    Menu, daemonSubMenu, Icon, Force quit daemon, %featherIcons%, 275, 24

    ; initialise the main menu item
    Try {
        Menu, Tray, Insert, Instance, %daemonMenu%, :daemonSubMenu
    }

    return daemonMenu
}

DeleteDaemonMenu() {
    Try {
        Menu, Tray, Delete, Unknown
    }
    Try {
        Menu, Tray, Delete, Daemon stopped
    }
    Try {
        Menu, Tray, Delete, Daemon loading...
    }
    Try {
        Menu, Tray, Delete, Daemon running
    }
}

EmacsServerExists() {
    if ProcessExist("emacs.exe") {

        Try {
            RunWait, %emacsclient% -a false -e "(daemonp)", , Hide UseErrorLevel ; check if daemon first (servers can be run from an instance as non-daemon also)
            return "daemon-server"
        }
        Catch {
            Try {
                RunWait, %emacsclient% -a false -e "t", , Hide UseErrorLevel ; if the process responds to an emacsclient request, it's a server
                return "instance-server"
            }
            Catch {
                return 0
            }
        }
    }
    else {
        return 0
    }
}

GetDaemonStatus() {

    if ProcessExist(emacsDaemonPid) { ; is it a process assist launched or reclaimed
        ; get a list of windows for the daemon
        WinGet, currentDaemonWindow, List, emacsDaemonPid
        
        if !(currentDaemonWindow) { ; if it has no windows it could be loading 
            
            if EmacsServerExists() { ; check to see if the server is responding yet, if yes it's probably just an existing daemon without any active windows
                MsgBox check: Running!
                return "running"
            }
            else {
                MsgBox check: loading - no window
                GoSub, Loading
            }
        }
        else { ; if it doesn't have any active windows, and is not responding to emacsclient it's likely to be loading (or crashed!)
            MsgBox, check: loading - other
            GoSub, Loading
        }
    }
    else if EmacsServerExists() { ; if we dont get a response from emacsclient
        MsgBox check: old Daemon!
        return "oldDaemon"
    }
    else if ProcessExist("emacs.exe") { ; TODO: implement 'if multiple emacs.exe running'
  
        ; get the process pid for use later
        procPid := "ahk_pid" . ProcessExist("emacs.exe")
        ; get a list of windows for the process
        WinGet, procWindow, List, procPid
        
        if WinExist(procPid) { ; if process has window, is not responding to emacsclient it's likely to be an instance
            MsgBox check: instance!
            return "instance"
        }
        else { 
            try { ; try waiting for the program to load
                MsgBox check: loading - proc
                GoSub, Loading
            }
            catch e { ; otherwise 
                MsgBox check: unknown!
                return "unknown"
            }
        }
    }
    else { 
        return "stopped"
    }
    
    Loading:
        UpdateDaemonHealth("loading")
        WinWait ahk_pid %procPid%, , 3 ; wait up to 3 seconds for a window to appear, then repeat check
        return GetDaemonStatus()
            
}

ReclaimOldDaemon() {
    if (GetDaemonStatus() = "oldDaemon") {

        ; get the pid of the process
        procPid := ProcessExist("emacs.exe")
        
        if procPid { ; dont overwrite existing values unless procPid actually exists
            ; set emacsDaemonPid
            emacsDaemonPid = %procPid%

            ; over-write the ini file value
            IniWrite, %emacsDaemonPid%, %iniFile%, SessionVariables, lastEmacsDaemonPid

            ; attempt to get status with new pid & send success message 
            if (GetDaemonStatus() = "running") {
                UpdateDaemonHealth(GetDaemonStatus())
                AssistToast("reclaimed previous daemon [" emacsDaemonPid "]")
                return emacsDaemonPid
            }
            else {
                AssistToast("a daemon from a previous session was detected, but could not be reclaimed")
                return
            }
        }
        else {
            AssistToast("no previous daemon process was detected")
            return
        }
    }
    else {
        AssistToast("no previous daemon process was found")
        return
    }
}

UpdateDaemonHealth(daemonStatus) {

    DeleteDaemonMenu() 

    if (daemonStatus = "running") {
        menuName = Daemon running
        CreateDaemonMenu(menuName)
        Menu, Tray, Icon, %menuName%, %featherIcons%, 45, 24
        return menuName
    }

    if (daemonStatus = "stopped") {
        menuName = Daemon stopped
        CreateDaemonMenu(menuName)
        Menu, Tray, Icon, %menuName%, %featherIcons%, 56, 24
        return menuName
    }

    if (daemonStatus = "oldDaemon") {
        try { ; attempt to reclaim if old daemon
            ReclaimOldDaemon()
            return "reclaimed"
        }
        catch {
            UpdateDaemonHealth("stopped")
        }
    }

    if (daemonStatus = "loading") {
        menuName = Daemon loading...
        Menu, Tray, Insert, Instance, %menuName%, Cancel
        Menu, Tray, Disable, %menuName%
        Menu, Tray, Icon, %menuName%, %featherIcons%, 1, 24
        return menuName
    }

    if (daemonStatus = "instance") {
        UpdateDaemonHealth("stopped")
    }

    if (daemonStatus = "unknown") {
        menuName = Unknown
        Menu, Tray, Insert, Instance, %menuName%, Cancel
        Menu, Tray, Disable, %menuName%
        Menu, Tray, Icon, %menuName%, %featherIcons%, 127, 24
        return menuName
    }
}

ExitFunc() {
    
    ; quit any active daemon (prompting to save for work)
    QuitDaemon("saveQuit")
    
    ; make sure the tray icon doesn't hang about afterwards 
    Menu, Tray, NoIcon

}

;; create helper

; load settings from ini file or generate new
AssistLoadSettings()

; bind exit function to OnExit hook
OnExit("ExitFunc")

; check daemon status each time the tray is right-clicked (so that user always sees most up-to-date status)
OnMessage(0x404, "AHK_NOTIFYICON")
AHK_NOTIFYICON(wParam, lParam)
{
    if (lParam = 0x205) ; WM_RBUTTONDOWN
    {
        UpdateDaemonHealth(GetDaemonStatus())
    }
}

; add a heading to the menu
Menu, Tray, Add, %menuTitle%, Cancel
Menu, Tray, Disable, %menuTitle%
Menu, Tray, Icon, %menuTitle%, %featherIcons%, 284, 24

Menu, Tray, Add ; separator

Menu, Tray, Add, New frame, OpenEmacsFrame
Menu, Tray, Icon, New frame, %featherIcons%, 72, 24
Menu, Tray, Default, New frame ; set the default action to open new frame

Menu, Tray, Add, Update health, UpdateHealth
Menu, Tray, Icon, Update health, %featherIcons%, 1, 24

Menu, Tray, Add, Delete health, DeleteHealth
Menu, Tray, Icon, Delete health, %featherIcons%, 1, 24

Menu, Tray, Add, Change window icon, ChangeWindowIcon
Menu, Tray, Icon, Change window icon, %featherIcons%, 8, 24

Menu, Tray, Add ; separator

; create the instance submenu
Menu, instanceSubMenu, Add, Launch new instance, LaunchInstance
Menu, instanceSubMenu, Icon, Launch new instance, %featherIcons%, 189, 24
Menu, instanceSubMenu, Add, Kill all instances (incl. daemon), KillAllEmacs
Menu, instanceSubMenu, Icon, Kill all instances (incl. daemon), %featherIcons%, 276, 24
Menu, Tray, Add, Instance, :instanceSubMenu
Menu, Tray, Icon, Instance, %featherIcons%, 132, 24

Menu, Tray, Add ; separator

; create the assist-helper submenu
Menu, helperSubMenu, Add, Suspend helper, SuspendApp
Menu, helperSubMenu, Icon, Suspend helper, %featherIcons%, 175, 24
Menu, helperSubMenu, Add, Reload helper, ReloadScript
Menu, helperSubMenu, Icon, Reload helper, %featherIcons%, 196, 24

Menu, helperSubMenu, Add, ; separator

Menu, helperSubMenu, Add, Settings, OpenSettings
Menu, helperSubMenu, Icon, Settings, %featherIcons%, 208, 24
Menu, helperSubMenu, Add, About, About
Menu, helperSubMenu, Icon, About, %featherIcons%, 132, 24

Menu, Tray, Add, assist-helper, :helperSubMenu
Menu, Tray, Icon, assist-helper, %featherIcons%, 283, 24
Menu, Tray, Add, Exit
Menu, Tray, Icon, Exit, %featherIcons%, 146, 24

; finally, get daemon status & populate the appropriate menu
UpdateDaemonHealth(GetDaemonStatus())

Return

; associated labels/subroutines for tray icon menu entries

Cancel:
    Return
    
GetExecutablePaths: ; retrieve full paths to emacs executables (assumes all in same dir as emacs.exe)
    global emacs := emacs_dir "\bin\emacs.exe"
    global runemacs := emacs_dir "\bin\runemacs.exe"
    global emacsclient := emacs_dir "\bin\emacsclient.exe"
    global emacsclientw := emacs_dir "\bin\emacsclientw.exe"
    Return
    
OpenEmacsFrame:
    NewFrame()
    GetDaemonStatus()
    ;ChangeWindowIcon(emacsDaemonPid)
    Return

StartDaemon:
    StartDaemon()
    ;ChangeWindowIcon(emacsDaemonPid)
    Return

QuitDaemon:
    QuitDaemon("saveQuit")
    Return
    
ChangeWindowIcon:
    ChangeWindowIcon(emacsDaemonPid, assistIcon)
    Return

ForceQuitDaemon:
    QuitDaemon("forceQuit")
    Return

KillAllEmacs:
    KillAllProcess("emacs.exe")
    UpdateDaemonHealth(GetDaemonStatus())
    Return

LaunchInstance:
    LaunchInstance()
    Return

OpenSettings:
    EditConfig()
    Return

SuspendApp:
    Suspend
    Return

ReloadScript:
    Reload
    Return

GenerateConfig:
   GenerateConfig(iniFile)
   Return
    
;; debug
UpdateHealth:
    UpdateDaemonHealth(GetDaemonStatus())
    Return

DeleteHealth:
    DeleteDaemonMenu()
    Return

About:
    Return

Exit:
    ExitApp

;; set global shortcuts:
    ; disable caps lock
    CapsLock::  
    ; WIN + 0 - global org-capture
    <#0::Run, %emacsclientw% -n -c -F "((name . \"org-capture\") (left-fringe . 0) (right-fringe . 0) (border-width . 1) (internal-border-width . 5) (width . 150) (height . 25) (left . 0.5) (top . 0.5) (transient . t) (undecorated . t) (unsplittable . t) (auto-raise . t) (alpha . 0.85))" -e "(+org-capture/open-frame nil \"zc\")" -a ''
    