LoadSettings() {
    For index, var in envVars {
        ; read the variable in question into a temp variable from ini file
        IniRead, iniVal, %iniFile%, EnvironmentVariables, %var%, % envDefs[A_Index] ; expression syntax to access array

        ; only try and grab environment variable if IniRead failed & fell back to default value
        if (iniVal = envDefs[A_Index] and not %var%) {
            ; attempt to retrieve existing environment variable
            EnvGet, envVal, %var%
            if (envVal) {
                 iniVal = %envVal% ; if envVal exists, replace iniVal
            } 
        }
        ; expand any variables within variables
        Transform, %var%, deref, %iniVal%

        ; set as an environment variable to allow launched programs to inherit
        EnvSet, %var%, % %var%

        ; debug
        MsgBox % "index: " index "`nvar: " var "`ndef: " envDefs[A_Index] "`niniVal: " iniVal "`nenvVal: " envVal "`nfinalVal: " %var%
    }
}

CheckDaemon: ; check if daemon first (servers can be run from an instance as non-daemon also)
    ErrorLevel = 0
    RunWait, %emacsclient% -a false -e "(daemonp)", , Hide UseErrorLevel
    if (ErrorLevel) {
        throw
    }
    else return "daemon-server"
    
        
CheckInteractiveServer: ; if the process responds to an emacsclient request, it's like to be an interactive server instance
    ErrorLevel = 0
    RunWait, %emacsclient% -a false -e "t", , Hide UseErrorLevel 
    if (ErrorLevel) {
        throw
    }
    else return "instance-server"


IsServer(procOrPid:="emacs.exe") {

CheckEmacsProcExist(procOrPid)

GoSub CheckLoading

Try GoSub CheckDaemon
Catch {
    Try GoSub CheckInteractiveServer
    Catch {
        AssistToast("emacs is running, but no server was detected: check that EMACS_SERVER_FILE is set in assist-config")
        IsInstance(procOrPid)
        return false
    }
}

CheckLoading:
    Loop {
        IsEmacsLoading()
    } Until not IsEmacsLoading()


}

IsInstance(procOrPid:="emacs.exe") {
    if !ProcessExist(procOrPid) {
        if !IsServer() {
            return "instance"
        }
    }
    else {
        AssistToast("emacs process [" procOrPid "] not found")
        return false
    }
}

GetDaemonStatus() {

    if ProcessExist(emacsDaemonPid) { ; is it a process assist launched or reclaimed

        
        if !(currentDaemonWindow) { ; if it has no windows it could be loading 
            
            if IsServer() { ; check to see if the server is responding yet, if yes it's probably just an existing daemon without any active windows
                MsgBox check: Running!
                return "running"
            }
            else {
                MsgBox check: loading - no window
                GoSub, Loading
            }
        }
        else { ; if it doesn't have any active windows, and is not responding to emacsclient it's likely to be loading (or crashed!)
            MsgBox, check: loading - other
            GoSub, Loading
        }
    }
    else if IsServer() { ; if we dont get a response from emacsclient
        MsgBox check: old Daemon!
        return "oldDaemon"
    }
    else if ProcessExist("emacs.exe") { ; TODO: implement 'if multiple emacs.exe running'
  
        ; get the process pid for use later
        procPid := "ahk_pid" . ProcessExist("emacs.exe")
        ; get a list of windows for the process
        WinGet, procWindow, List, procPid
        
        if WinExist(procPid) { ; if process has window, is not responding to emacsclient it's likely to be an instance
            MsgBox check: instance!
            return "instance"
        }
        else { 
            try { ; try waiting for the program to load
                MsgBox check: loading - proc
                GoSub, Loading
            }
            catch e { ; otherwise 
                MsgBox check: unknown!
                return "unknown"
            }
        }
    }
    else { 
        return "stopped"
    }
    
    Loading:
        UpdateDaemonHealth("loading")
        WinWait ahk_pid %procPid%, , 3 ; wait up to 3 seconds for a window to appear, then repeat check
        return GetDaemonStatus()
            
}

ReclaimOldDaemon() {
    if (GetDaemonStatus() = "oldDaemon") {

        ; get the pid of the process
        procPid := ProcessExist("emacs.exe")
        
        if procPid { ; dont overwrite existing values unless procPid actually exists
            ; set emacsDaemonPid
            emacsDaemonPid = %procPid%

            ; over-write the ini file value
            IniWrite, %emacsDaemonPid%, %iniFile%, SessionVariables, lastEmacsDaemonPid

            ; attempt to get status with new pid & send success message 
            if (GetDaemonStatus() = "running") {
                UpdateDaemonHealth(GetDaemonStatus())
                AssistToast("reclaimed previous daemon [" emacsDaemonPid "]")
                return emacsDaemonPid
            }
            else {
                AssistToast("a daemon from a previous session was detected, but could not be reclaimed")
                return
            }
        }
        else {
            AssistToast("no previous daemon process was detected")
            return
        }
    }
    else {
        AssistToast("no previous daemon process was found")
        return
    }
}

global emacsPid ; initialise to track process id for daemon
global lastEmacsMenuName = Emacs ; the name of the menu in the most recent emacs menu update

#Include helper-fns.ahk


;;; messing around with regex to try and make netstat lookup faster
;GetEmacsPids()
MsgBox % inStr(GetTrimCmdOutput("netstat -anop tcp"), "6752")
MsgBox % TestCmd()

GetEmacsPids() {

    global emacsPidsArr := GetPidsByProcName("emacs.exe")
    global emacsServerPids := []
    global emacsInstancePids := []
    
    For pid in emacsPidsArr {
        if IsEmacsInstance(emacsPidsArr[pid]) {
            emacsInstancePids.Push(emacsPidsArr[pid])
        }
        else emacsServerPids.Push(emacsPidsArr[pid])
    }
    
    return
}


    
    servers[pid] {
        get {
            if pid {
                Return this._servers[pid]
            }
            Else Return this._servers
        }
        set {

            If !value { ; populates with standard info (ip, port etc)
                Try isServer()
                Catch {
                    Return false ; if no server info, not a server
                }
                Return true ; isServer() will write serverInfo to the pid's entry in object
                }
            Else { ; allows overriding pid's value
                Return this._servers[pid] := value
            }
        }
                
    }



;Menu, assistMenu, Add, Suspend, SuspendAssist
;Menu, Tray, Add, % ">> assist-helper", :assistMenu
;tray.add(">> assist-helper", ":assistMenu")
;tray.add("something", {"item1": [Func("someTest").Bind("other"), 277]}, {"item2": [Func("someTest").Bind("other"), 232]}, {"item3": [Func("someTest").Bind("other"), 255]})
;Sleep 5000
;tray.enumItems()
;tray.add(">> assist-helper").submenu("Suspend", "SuspendAssist", 175)
;tray.submenu(">> assist-helper").add("Suspend", "SuspendAssist", 175)
;tray.add("Suspend", "SuspendAssist", 175)

    ;rename(menuItem, newName) {
        ;Menu, % this.items[menuItem]["parent"], Rename, % menuItem, % newName
        ;this.items[newName] := this.items[menuItem], this.items.Remove(menuItem) ; reflect the change in assoc array
        ;Return this.items[newName]
    ;}
    
    ;items[menuItem:="", newName:=""] {
        ;get {
            ;_menuItem := {}
            ;For prop, value in this._items[menuItem] {
                ;_menuItem[prop] := value
            ;}
            ;Return _itemRes
        ;}
    ;}
    
    ; items[menuItem] {
    ;     get {
    ;         Return this._items[menuItem]
    ;     }
    ;     set {
    ;         For prop, value in this._items[menuItem] {
    ;             this._items[menuItem][prop]
    ;         }
    ;     }
    ; }
    
    ;items[menuItem:="", newName:=""] {
        ;get {
        ;
            ;If newName { ; rename item without losing values
                ;this._items[newName] := this._items[menuItem], this._items.Remove(menuItem) ; rename in assoc array
                ;MsgBox % this._items[menuItem]["parent"]
                ;Menu, % this._items[newName]["parent"], Rename, % menuItem, % newName
                ;menuItem := newName
            ;}
            ;
            ;If (!menuItem) {
                ;Return this._items
            ;}
            ;Else {
                ;Return this._items[menuItem]
            ;}
            ;
        ;}
        ;set {
        ;
            ;itm := this._items[menuItem] ; make shorthand
                      ;
            ;If IsObject(value) {
                ;For k, v in value {
                    ;itm[k] := v
                ;}
                 ;;add is effectively an update function too - use it to refresh each time we set
                ;;this.add(menuItem, itm[funcOrSubmenu], itm[iconIndex], itm[opts], itm[parent])
            ;}
            ;Else If (value := ""){
                ;itm := value
                ;this.del()
            ;}
            ;Else {
                ;itm := value
            ;}
            ;
             ;;add is effectively an update function too - use it to refresh each time we set
            ;;this.add(menuItem, itm["funcOrSubmenu"], itm["iconIndex"], itm["opts"], itm["parent"])
            ;
            ;Return this._items
        ;}
    ;}