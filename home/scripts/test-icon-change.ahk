﻿#Persistent 
Ico := A_ScriptDir "\assist.ico" 
;IfNotExist,%ICO%, URLDownloadToFile,https://raw.githubusercontent.com/gura-lang/gura/master/sample/resource/msnms.ico,%ICO%
;Menu,Tray,Icon,%Ico%

Run, Notepad.exe,,, PID
Winwait, ahk_pid %PID%
ID := WinExist("ahk_pid" PID)

hIcon := DllCall( "LoadImage", UInt,0, Str,Ico, UInt,1, UInt,0, UInt,0, UInt,0x10 )
SendMessage, 0x80, 0, hIcon ,, ahk_id %ID%  ; One affects Title bar and
SendMessage, 0x80, 1, hIcon ,, ahk_id %ID%  ; the other the ALT+TAB menu
SendMessage, 0x80, 0, hIcon ,, ahk_id %ID%  ; Small Icon
SendMessage, 0x80, 1, hIcon ,, ahk_id %ID%  ; Big Icon
SendMessage, WM_SETICON:=0x80, ICON_SMALL2:=0, hIcon,, ahk_id %hWnd%  ; Set the window's small icon
SendMessage, WM_SETICON:=0x80, ICON_BIG:=1   , hIcon,, ahk_id %hWnd%  ; Set the window's big icon to the same one.

SendMessage,


SetTimer, CheckID, 1000
Return

CheckID:
 If ! WinExist( "ahk_id" ID )
    ExitApp
Return