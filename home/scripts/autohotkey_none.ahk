;Script to make Temporary File Association for Ahk Files
;Double click on script opens it with the AutoHotkey.exe path you select on running
;Right click on scripts opens a custom menu for edit and stuff.
;Made by None for people who like to run portable

;Make Some Menus
Menu, Tray, NoStandard 
Menu, Tray, Add, Change Path, ChangePath 
Menu, Tray, Add
Menu, Tray, Add, Exit, ExitScript 
Menu, Tray, Default, Exit ;Double click on Icon to exit
Menu, ahkMenu, Add, Run, RunIt
Menu, ahkMenu, Add, Edit, OpenIt 
Menu, ahkMenu, Add, 
Menu, ahkMenu, Add, Copy, CopyIt 
Menu, ahkMenu, Add, Delete, DeleteIt 

SetWorkingDir %A_ScriptDir%
IniRead, AhkPath, AhkPath.ini, AutoHotkey Path, Path ;read path to ahk from IniFile

IfNotExist, %AhkPath% ;if path not valid
 AhkPath:=GetPath() ;get new path
If (AhkPath="Cancled") ;if you cancel the find ahk and there is no previous path exit
 ExitApp
SetTimer CheckUnderMouse,200
Return ;end of Auto Execute

CheckUnderMouse:
MouseGetPos ,,, OutputVarWin
WinGetClass, OutputVar, ahk_id %OutputVarWin%
if OutputVar contains Progman,CabinetWClass,ExploreWClass
 {
 If A_IsSuspended
  Suspend Off
 }
Else
 {
 If !A_IsSuspended
  Suspend On
 }
Return

$LButton::
If (A_TimeSincePriorHotkey>400 || A_PriorHotkey<>A_ThisHotkey)
 {
 Send {LButton Down}
 KeyWait LButton
 Send {LButton Up}
 Return
 }
FilePath:=GetFile()
SplitPath, FilePath,,Dir,Ext
If (Ext="ahk")
 Run %AhkPath% "%FilePath%",%Dir%
Else
 Send {LButton} ;second Click of Double
Return

$Rbutton::
Send {LButton} ;to select the file under mouse
FilePath:=GetFile()
SplitPath, FilePath,,Dir,Ext
If (Ext="ahk")
 Menu, ahkMenu, Show
Else
 {
 Send {RButton Down}
 KeyWait RButton
 Send {RButton Up}
 Return
 }
Return

ChangePath:
AhkPath:=GetPath(AhkPath) ;if Cancled previous path used
Return

ExitScript:
ExitApp

RunIt:
Run %AhkPath% "%FilePath%",%Dir%
Return
Openit:
Run Notepad.exe "%FilePath%"
Return
CopyIt:
Send ^c
Return
DeleteIt:
FileRecycle, %FilePath% 
Return

GetPath(Old="Cancled") {
FileSelectFile, tempvar , 3, %A_ScriptDir%, Find AutoHotkey.exe, AutoHotkey.exe
If !ErrorLevel
 {
 IniWrite, %tempvar%, AHKpath.ini, AutoHotkey Path, Path
 Return tempvar
 }
Return Old
}

GetFile() {
ClipSave:=ClipboardAll
Clipboard:=""
Send ^c
ClipWait, .2
Path=%clipboard%
Clipboard:=ClipSave
Return Path
}