﻿iniFile=C:\Users\Hassan\Development\werk\home\scripts\assist-config.ini
thingy := ["emacs_dir=%derived_emacs_dir%", "HOME=%USERPROFILE%", "EMACSDIR=%HOME%\.emacs.d", "EMACS_SERVER_FILE=%EMACSDIR%\.local\cache\server\server"]


; read EnvironmentVariables section into a multi-line string var
;IniRead, ini, %iniFile%, EnvironmentVariables

varList := {}

; convert multi-line string to array (strip spaces from beginning and end) 
;envVars := StrSplit(ini, "`n", " `t")

; loop through and set as environment variables
For v in thingy
{
	RegExMatch(thingy[v], "(\S+)\s?=\s?(.*)", var)
    
	;%var1% = %var2%
    EnvSet, %var1%, %var2%
    
    varList[(var1)] := (var2)
}

MsgBox Done!